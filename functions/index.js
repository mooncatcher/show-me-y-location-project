

const admin = require('firebase-admin');
const functions = require('firebase-functions');

admin.initializeApp(functions.config().firebase);

exports.sendNotification = functions.database.ref("Notifications")
.onWrite(event => {
	var request = event.data.val();
	
	var payload ={
		notification: {
            
        },
	};
	
	admin.messaging().sendToDevice(request.token, payload)
	.then(function(response){
		console.log("Sent! ", response);
	})
	.catch(function(error){
		console.log("Error! ", error);
	})
})

// Listens for new messages added to messages/:pushId
exports.pushNotification = functions.database.ref('/notificationRequests/{pushId}').onWrite( event => {

    console.log('Push notification event triggered');

    //  Grab the current value of what was written to the Realtime Database.
    var valueObject = event.data.val();


  // Create a notification
    const payload = {
        notification: {
            title: valueObject.message,
            body: valueObject.body,
            sound: "default"
        },
    };
	
	const token = valueObject.PhoneNumber;

  //Create an options object that contains the time to live for the notification and the priority
    const options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    };

	
	return admin.messaging().sendToTopic(token, payload);
	
});