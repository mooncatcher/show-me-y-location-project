package com.example.ketcher.show_me_y_location_project;
import android.os.StrictMode;
import android.util.Log;
import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static java.util.jar.Attributes.Name.CONTENT_TYPE;
/**
 * Created by Ketcher on 05.08.2017.
 */
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIdService.class.getSimpleName();
    @Override
    public void onTokenRefresh() {
        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "Refreshed token " + refreshToken);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifications");
        reference.child("Ftoken").setValue(refreshToken);
    }
    public String getToken(){
        return FirebaseInstanceId.getInstance().getToken();
    }
    public String getId(){
        return FirebaseInstanceId.getInstance().getId();
    }
}
