package com.example.ketcher.show_me_y_location_project;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.firebase.client.Firebase;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by Ketcher on 14.08.2017.
 */
public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final int RC_SIGN_IN = 1;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mMessagesDatabaseReferences;
    private ChildEventListener mChildEventListener;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    ProgressDialog mProgressiveDialog;
    String userNumber = "";
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    private static final int GALLERY_INTENT = 2;
    boolean isAuthenticated = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        isAuthenticated = FirebaseAuth.getInstance().getCurrentUser() != null;
        mProgressiveDialog = new ProgressDialog(this);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final NavigationView navView = (NavigationView) findViewById(R.id.nav_view);
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int screenHeight = getResources().getDisplayMetrics().heightPixels;
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navView.getLayoutParams();
        params.width = screenWidth;
        params.height = screenHeight;
        navView.setLayoutParams(params);
        if(FirebaseAuth.getInstance().getCurrentUser() != null) {
            GPSTrackerHelper gpsTrackerHelper = new GPSTrackerHelper(this, "WIFI");
            Location wifiLocation = gpsTrackerHelper.getLocation("WIFI");
            gpsTrackerHelper.getLocation("GPS");
        }
        LinearLayout thatLayout = (LinearLayout) findViewById(R.id.thatLayout);
        Button changeDisplayname = (Button) findViewById(R.id.But1);
        Button changeProfilePhoto = (Button) findViewById(R.id.But2);
        Button logOut = (Button) findViewById(R.id.But3);
        Button closeSettings = (Button) findViewById(R.id.closeSettings);
        TextView displayNumberTxt = (TextView) findViewById(R.id.displayNumberTxt);
        final ImageView displayPhoto = (ImageView) findViewById(R.id.displayPhoto);
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
            displayPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toGalery = new Intent(Intent.ACTION_PICK);
                    toGalery.setType("image/*");
                    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (cm.getActiveNetworkInfo() != null)
                        startActivityForResult(toGalery, GALLERY_INTENT);
                    else
                        Toast.makeText(getApplicationContext(), "No network connection", Toast.LENGTH_SHORT).show();
                }
            });
            mChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (mFirebaseAuth.getCurrentUser() != null) {
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                        Query applesQuery99 = mMessagesDatabaseReferences.
                                orderByChild("text").equalTo("DisplayPhoto");
                        applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                    Picasso.with(BaseActivity.this).load
                                            (value).fit().centerCrop().into(displayPhoto);
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener);
        }
        if (mFirebaseAuth.getCurrentUser() != null)
            displayNumberTxt.setText(mFirebaseAuth.getCurrentUser().getPhoneNumber());
        final TextView displayName = (TextView) findViewById(R.id.displayNameTxt);
        final DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(getApplicationContext());
        if (mDictionary.getContactsCount() > 0)
            displayName.setText(mDictionary.getContact(1).getName());
        String texxxt = displayName.getText().toString().replace("(You)", "");
        closeSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
            }
        });
        changeDisplayname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText edittext = new EditText(BaseActivity.this);
                final Button buttonGood = new Button(BaseActivity.this);
                final Button buttonCancel = new Button(BaseActivity.this);
                final RelativeLayout Rlayout = new RelativeLayout(BaseActivity.this);
                Rlayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                buttonGood.setText("Accept");
                buttonGood.setBackgroundColor(Color.TRANSPARENT);
                buttonGood.setTextColor(Color.parseColor("#b30000"));
                buttonGood.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buttonGood.getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                buttonCancel.setText("Cancel");
                buttonCancel.setBackgroundColor(Color.TRANSPARENT);
                buttonCancel.setTextColor(Color.parseColor("#b30000"));
                buttonCancel.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) buttonCancel.getLayoutParams();
                params1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                buttonGood.setLayoutParams(params);
                buttonCancel.setLayoutParams(params1);
                edittext.setText(displayName.getText().toString().replace("(You)", ""));
                edittext.setSelection(edittext.getText().toString().length());
                final LinearLayout parent = new LinearLayout(BaseActivity.this);
                ScrollView scrollView = new ScrollView(BaseActivity.this);
                scrollView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                parent.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                parent.setOrientation(LinearLayout.VERTICAL);
                parent.setPadding(36, 6, 36, 6);
                scrollView.addView(edittext);
                Rlayout.addView(buttonGood);
                Rlayout.addView(buttonCancel);
                parent.addView(Rlayout);
                parent.addView(scrollView);
                builder = new AlertDialog.Builder(BaseActivity.this);
                alertDialog = builder.create();
                alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                alertDialog.setView(parent);
                alertDialog.setCancelable(false);
                buttonGood.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String texxxt = displayName.getText().toString().replace("(You)", "");
                        if (texxxt.equals(edittext.getText().toString())) {
                            alertDialog.dismiss();
                        } else {
                            displayName.setText(edittext.getText().toString() + "(You)");
                            DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(getApplicationContext());
                            SQLiteDatabase dbWrite = mDictionary.getWritableDatabase();
                            SQLiteDatabase dbRead = mDictionary.getReadableDatabase();
                            Message message = new Message(edittext.getText().toString() + "(You)",
                                    FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                            mDictionary.Clear();
                            mDictionary.addContact(message);
                            mFirebaseDatabase = FirebaseDatabase.getInstance();
                            mMessagesDatabaseReferences = mFirebaseDatabase.getReference().
                                    child(mFirebaseAuth.getCurrentUser().getPhoneNumber().substring(3));
                            Query applesQuery1 = mMessagesDatabaseReferences.
                                    orderByChild("text").equalTo("DisplayName");
                            applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                        alertDialog.dismiss();
                    }
                });
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
        changeProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toGalery = new Intent(Intent.ACTION_PICK);
                toGalery.setType("image/*");
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                if (cm.getActiveNetworkInfo() != null)
                    startActivityForResult(toGalery, GALLERY_INTENT);
                else
                    Toast.makeText(getApplicationContext(), "No network connection", Toast.LENGTH_SHORT).show();
            }
        });
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                AuthUI.getInstance().signOut(BaseActivity.this);
            }
        });
        if (FirebaseAuth.getInstance().getCurrentUser() != null)
            userNumber = mFirebaseAuth.getCurrentUser().getPhoneNumber().substring(3);
        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(userNumber);
        Query applesQuery = mMessagesDatabaseReferences.
                orderByChild("text").equalTo("DisplayName");
        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int klmn = 0;
                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                    klmn = 1;
                    String newDisplayName = appleSnapshot.getValue().toString().
                            replace("text=DisplayName, name=", "").replace("{", "").replace("}", "" + "(You)");
                    DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(getApplicationContext());
                    if (!displayName.getText().toString().replace("(You)", "").equals(mDictionary.getContact(1).getName())) {
                        displayName.setText(mDictionary.getContact(1).getName());
                    }
                    if (!newDisplayName.replace("(You)", "").equals(displayName.getText().toString().replace("(You)", ""))) {
                        appleSnapshot.getRef().removeValue();
                        Message message = new Message("DisplayName", mDictionary.getContact(1).getName().replace("(You)", ""));
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                        mMessagesDatabaseReferences.push().setValue(message);
                    }
                    displayName.setText(mDictionary.getContact(1).getName());
                }
                if (klmn != 1) {
                    Intent settingName = new Intent(getApplicationContext(), SettingDisplayName.class);
                    startActivity(settingName);
                    finish();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        Firebase.setAndroidContext(this);
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (FirebaseAuth.getInstance()
                            .getCurrentUser() != null && !FirebaseAuth.getInstance()
                            .getCurrentUser()
                            .getPhoneNumber().equals("")) {
                        onSignedInInitialize(user.getPhoneNumber().substring(3));
                        if (!isAuthenticated) {
                            Intent mapIntent = new Intent(getApplicationContext(), MapActivity.class);
                            startActivity(mapIntent);
                        }
                    }
                } else {
                    //user is signed out
                    onSignedOutCleanup();

                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setAvailableProviders(
                                            Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.PHONE_VERIFICATION_PROVIDER).build()))
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };
    }
    private void onSignedOutCleanup() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            userNumber = mFirebaseAuth.getCurrentUser().getPhoneNumber().substring(3);
            FirebaseMessaging.getInstance().unsubscribeFromTopic(userNumber.replaceAll("[^0-9]", "").replaceAll(" ", ""));
        }
    }
    private void onSignedInInitialize(String username) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
            FirebaseMessaging.getInstance().subscribeToTopic(userNumber.replaceAll("[^0-9]", "").replaceAll(" ", ""));
            userNumber = mFirebaseAuth.getCurrentUser().getPhoneNumber().substring(3);
            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
            mMessagesDatabaseReferences.addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.hasChild(userNumber)) {
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                        Message message = new Message("kurva!", "Jestem,");
                        mMessagesDatabaseReferences.push().setValue(message);
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }
    public void attachDatabaseReadListener() {
        if (mChildEventListener == null) {
            mChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Message message = dataSnapshot.getValue(Message.class);
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }
    @Override
    protected void onPause() {
        super.onPause();
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings_menu, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                AuthUI.getInstance().signOut(this);
                return true;
        }
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
        }
        return true;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_INTENT && resultCode == RESULT_OK) {
            mProgressiveDialog.setTitle("Please wait...");
            mProgressiveDialog.setMessage("Uploading Your profile photo");
            mProgressiveDialog.setCanceledOnTouchOutside(false);
            mProgressiveDialog.show();
            Uri uri = data.getData();
            StorageReference filepath = FirebaseStorage.getInstance().getReference()
                    .child("Photos").child(uri.getLastPathSegment());
            StorageReference mStorageRefences = FirebaseStorage.getInstance().getReference();
            //changeProfilePhoto
            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Query applesQuery1 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("DisplayPhoto");
                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                String mImageUrl = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                StorageReference photoRef = FirebaseStorage.getInstance().getReferenceFromUrl(mImageUrl);
                                photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        // File deleted successfully
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Uh-oh, an error occurred!
                                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    mProgressiveDialog.dismiss();
                    Uri downloadUri = taskSnapshot.getDownloadUrl();
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                    Query applesQuery99 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("DisplayPhoto");
                    applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    Message message = new Message("DisplayPhoto", downloadUri.toString());
                    mMessagesDatabaseReferences.push().setValue(message);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        }
    }
}
