package com.example.ketcher.show_me_y_location_project;

import android.app.Application;
import android.content.Context;

/**
 * Created by Ketcher on 19.12.2017.
 */

public class  App extends Application {
    private static Context context;
    private static DBHelper dbHelper;

    @Override
    public void onCreate()
    {
        super.onCreate();
        context = this.getApplicationContext();
        dbHelper = new DBHelper(context);
        DatabaseManager.initializeInstance(dbHelper);

    }


    public static Context getContext(){
        return context;
    }

}
