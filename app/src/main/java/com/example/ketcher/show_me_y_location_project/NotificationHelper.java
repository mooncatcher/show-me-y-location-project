package com.example.ketcher.show_me_y_location_project;
import android.support.annotation.Nullable;
import android.util.Log;
import com.firebase.client.annotations.NotNull;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Ketcher on 25.11.2017.
 */
public class NotificationHelper {
    private static final String TAG = "NotificationHelper>>";
    private DatabaseReference messagesDatabaseReferences;
    public void sendNotificationToUser(final String destPhoneNumber, final String requestType) {
        final FirebaseUser loggedUser = FirebaseAuth.getInstance().getCurrentUser();
        if (loggedUser == null) {
            Log.e(TAG, "sendNotificationToUser firebaseUser is null, return");
            return;
        }
        final String phoneNumber = loggedUser.getPhoneNumber();
        if (phoneNumber == null) {
            Log.e(TAG, "sendNotificationToUser phoneNumber is null, return");
            return;
        }
        messagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                .child(phoneNumber.substring(3));
        Query usernameQuery = messagesDatabaseReferences.
                orderByChild("text").equalTo("DisplayName");
        usernameQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {


                    String displayedName = snapshot.getValue()
                            .toString()
                            .replace("text=DisplayName", "")
                            .replace("name=", "")
                            .replace("}", "")
                            .replace("{", "")
                            .replace(", ", "");
                    messagesDatabaseReferences = FirebaseDatabase.getInstance()
                            .getReference()
                            .child("notificationRequests");
                    Map<String, Object> notificationMap = new HashMap<>();
                    String user1;
                    if (destPhoneNumber.contains("+")) {
                        user1 = destPhoneNumber.substring(3);
                    } else
                        user1 = destPhoneNumber;



                    notificationMap.put("PhoneNumber", user1.replaceAll("[^0-9]", ""));
                    notificationMap.put("message", displayedName + "/" +
                            phoneNumber.substring(3));
                    notificationMap.put("requestType", requestType);
                    notificationMap.put("timestamp", ServerValue.TIMESTAMP );
                    messagesDatabaseReferences.push().setValue(notificationMap);


                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
