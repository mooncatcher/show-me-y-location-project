package com.example.ketcher.show_me_y_location_project;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.location.GpsStatus;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Ketcher on 02.10.2017.
 */
public class MapActivity extends BaseActivity implements OnMapReadyCallback, GpsStatus.Listener {
    GoogleMap m_map;
    ImageView mySettings;
    MapFragment mapFragment;
    boolean mapReady = false;
    double currentLat;
    double currentLong;
    ProgressDialog dialog123;
    private LocationManager locationManager;
    boolean turnedGPSChecked = false;
    int contactsSize;
    int inAppContactsSize = 0;
    int outsizeAppContactsSize = 0;
    DataSnapshot dataSnapshot1;
    ChildEventListener mChildEventListener777;
    ChildEventListener mChildEventListener7777;
    private List<Message> messages = new ArrayList<>();
    private List<Message> messages2 = new ArrayList<>();
    private List<Message> messagesInApp = new ArrayList<>();
    private List<Message> messagesInApp2 = new ArrayList<>();
    private List<Message> outsideAnApp = new ArrayList<>();
    private List<Message> outsideAnApp2 = new ArrayList<>();
    private List<Message> messagesDB = new ArrayList<>();
    private List<Message> allConts = new ArrayList<>();
    private List<Message> allConts2 = new ArrayList<>();
    int statusBarHeight;
    private ChildEventListener mChildEventListener1;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mMessagesDatabaseReferences;
    private ChildEventListener mChildEventListener;
    private FirebaseAuth mFirebaseAuth;
    ChildEventListener mChildEventListener123;
    ArrayList<String> listItems = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";
    private static final String URL = "http://stackoverflow.com/feeds/tag?tagnames=android&sort=newest";
    // Whether there is a Wi-Fi connection.
    private static boolean wifiConnected = false;
    // Whether there is a mobile connection.
    private static boolean mobileConnected = false;
    // Whether the display should be refreshed.
    public static boolean refreshDisplay = true;
    // The user's current network preference setting.
    public static String sPref = null;
    MessageAdapter currentMessageAdapter;
    private MessageAdapter inAppmMessageAdapter;
    private MessageAdapter inAppmMessageAdapter2;
    RecyclerView inAppmMessageListView;
    ChildEventListener mChildEventListener12345;
    private  MessageAdapter outsideAppmMessageAdapter2;
    private MessageAdapter outsideAppmMessageAdapter;
    RecyclerView outsideAppmMessageListView;
    private MessageAdapter favoritesMessageAdapter;
    private MessageAdapter favoritesMessageAdapter2;
    RecyclerView favoritesListView;
    private List<Message> favoritesArray = new ArrayList<>();
    private List<Message> favoritesArray2 = new ArrayList<>();
    private List<Message> newItems = new ArrayList<>();
    String textq = "";
    // The BroadcastReceiver that tracks network connectivity changes.
    private NetworkReceiver receiver = new NetworkReceiver();
    //Local DB
    private static final String DATABASE_NAME = "ShowMeYLocProject";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
            getLayoutInflater().inflate(R.layout.main_map, contentFrameLayout);
            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("key", ""); //InputString: from the EditText
            editor.apply();



            onCreate2();


        }
        else {

            finish();
            Intent intent = new Intent(getApplicationContext(), AuthCheckActivity.class);
            startActivity(intent);

        }
    }


    public void onCreate2() {



        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) || (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) !=
                PackageManager.PERMISSION_GRANTED) || (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) && FirebaseAuth.getInstance().getCurrentUser() != null
                ) {
            askingPermissions1();
        } else {


            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);



            final RelativeLayout askLocLayout = (RelativeLayout) findViewById(R.id.askLocLayout);
            final TextView askLocLiketoAsk = (TextView) findViewById(R.id.askLocLiketoAsk);
            final Button refuseButton = (Button) findViewById(R.id.refuseButton);
            final Button allowButton = (Button) findViewById(R.id.allowButton);
            final TextView askLocName = (TextView) findViewById(R.id.askLocName);
            final TextView askLocNumber = (TextView) findViewById(R.id.askLocNumber);
            final ImageView ava = (ImageView) findViewById(R.id.askLocPicture);
            final ImageView searching = (ImageView) findViewById(R.id.searching);
            final LinearLayout searchingLayout = (LinearLayout) findViewById(R.id.searchLayout);
            ImageView clearBtn = (ImageView) findViewById(R.id.clearBtn);
            final EditText searchEdit = (EditText) findViewById(R.id.searchEdit);
            final ImageView addNew = (ImageView) findViewById(R.id.addNew);
            final LinearLayout newUserLayout = (LinearLayout) findViewById(R.id.newUserLayout);
            final LinearLayout newUserLayout2 = (LinearLayout) findViewById(R.id.newUserLayout2);
            ImageView clearPhone = (ImageView) findViewById(R.id.clearBtnPhone);
            ImageView clearName = (ImageView) findViewById(R.id.clearBtnName);
            final EditText newPhoneNumber = (EditText) findViewById(R.id.newPhoneNumber);
            final EditText newName = (EditText) findViewById(R.id.newName);
            final LinearLayout newUserLayoutMain = (LinearLayout) findViewById(R.id.newUserLayoutMain);
            Button cancelNewContact = (Button) findViewById(R.id.cancelNewContact);
            Button addNewContact = (Button) findViewById(R.id.addNewContact);
            final Button deleteContacts = (Button) findViewById(R.id.deleteContact);

            String fullPhoneNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
            String shortPhoneNumber = fullPhoneNumber.substring(fullPhoneNumber.length() - 9);

            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(shortPhoneNumber);

            mChildEventListener123 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        Query applesQuery99 = mMessagesDatabaseReferences.
                                orderByChild("text").equalTo("DisplayPhoto");
                        applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                    Picasso.with(getApplicationContext()).load
                                            (value).fit().centerInside().into(ava);
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);

            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
            mChildEventListener123 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot.hasChild(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location")) {
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                .child(askLocNumber.getText().toString());
                        Query applesQuery99 = mMessagesDatabaseReferences.
                                orderByChild("text").equalTo("DisplayPhoto");
                        applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                    Picasso.with(getApplicationContext()).load
                                            (value).fit().centerCrop().into(ava);
                                }
                                askLocLayout.setVisibility(View.VISIBLE);
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
            addNewContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addNewContact(newName.getText().toString(), newPhoneNumber.getText().toString());
                    messages.clear();
                    messagesInApp.clear();
                    outsideAnApp.clear();
                    displayContacts();
                    newPhoneNumber.setText("");
                    newName.setText("");
                }
            });
            cancelNewContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newUserLayoutMain.setVisibility(View.GONE);
                    addNew.setVisibility(View.VISIBLE);
                }
            });
            clearPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newPhoneNumber.setText("");
                }
            });
            clearName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newName.setText("");
                }
            });
            newName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newName.setText("");
                }
            });
            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
            }
            mChildEventListener123 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                    Query applesQuery99 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Allow share");
                    applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                                Message message123 = new Message("Allowed", "");
                                mMessagesDatabaseReferences.push().setValue(message123);
                                askLocLiketoAsk.setText("sharing currentLocation with");
                                allowButton.setText("Stop Sharing");
                                //tut
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                Query applesQuery99 = mMessagesDatabaseReferences.
                                        orderByChild("text").equalTo("number");
                                applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            final String number = appleSnapshot.getValue().toString()
                                                    .replace("{text=number, name=", "").replace("}", "");
                                            askLocNumber.setText(number);
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(number);
                                            Query applesQuery99 = mMessagesDatabaseReferences.
                                                    orderByChild("text").equalTo("DisplayName");
                                            applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(number);
                                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                        String name = appleSnapshot.getValue().toString()
                                                                .replace("{text=DisplayName, name=", "").replace("}","");
                                                        askLocName.setText(name);
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                                refuseButton.setVisibility(View.GONE);
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                        .child(askLocNumber.getText().toString());
                                Query applesQuery999 = mMessagesDatabaseReferences.
                                        orderByChild("text").equalTo("DisplayPhoto");
                                applesQuery999.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                            Picasso.with(getApplicationContext()).load
                                                    (value).fit().centerCrop().into(ava);
                                        }
                                        askLocLayout.setVisibility(View.VISIBLE);
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                            }
                                GPSTrackerHelper gpsTrackerHelper = new GPSTrackerHelper(getApplicationContext(), "WIFI");
                            }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
            clearBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!searchEdit.getText().toString().equals("")) {
                        searchEdit.setText("");
                    }
                    else {
                        searchingLayout.setVisibility(View.GONE);
                        searching.setVisibility(View.VISIBLE);
                    }
                }
            });
            addNew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchingLayout.setVisibility(View.GONE);
                    addNew.setVisibility(View.GONE);
                    searching.setVisibility(View.VISIBLE);
                    newUserLayoutMain.setVisibility(View.VISIBLE);
                }
            });
            searching.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchingLayout.setVisibility(View.VISIBLE);
                    searching.setVisibility(View.GONE);
                    addNew.setVisibility(View.VISIBLE);
                    newUserLayoutMain.setVisibility(View.GONE);
                }
            });
            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
            }
            refuseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                        userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                            Query applesQuery1 = mMessagesDatabaseReferences.
                                    orderByChild("PhoneNumber").equalTo(userNumber.replaceAll("[^0-9]", ""));
                            applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                        appleSnapshot.getRef().removeValue();
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                    askLocLayout.setVisibility(View.GONE);
                    askLocLiketoAsk.setText("ask for your currentLocation");
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(askLocNumber.getText().toString().replaceAll(" ", ""));
                    Query applesQuery199 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("number");
                    applesQuery199.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", ""));
                    Message message99 = new Message("Stop", "it!");
                    mMessagesDatabaseReferences.push().setValue(message99);
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                    Query applesQuery12 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Share loc");
                    applesQuery12.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                                askLocLayout.setVisibility(View.GONE);
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    Query applesQuery1234 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Allow");
                    applesQuery1234.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                                askLocLayout.setVisibility(View.GONE);
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            });
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(userNumber);
                mChildEventListener123 = new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(userNumber);
                        Query applesQuery133 = mMessagesDatabaseReferences.
                                orderByChild("text").equalTo("Allow");
                        applesQuery133.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(userNumber);
                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    final String number = appleSnapshot.getValue().toString().replace("{text=Allow, name=", "").replace("}", "");
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(number);
                                    Query applesQuery1 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("DisplayName");
                                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(number);
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                askLocName.setText(appleSnapshot.getValue().toString()
                                                        .replace("{text=DisplayName, name=", "").replace("}", ""));
                                                askLocNumber.setText(number);
                                                askLocLiketoAsk.setText("recieving currentLocation from");
                                                refuseButton.setVisibility(View.GONE);
                                                allowButton.setText("Stop Recieving");
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                        .child(askLocNumber.getText().toString());
                                                Query applesQuery99 = mMessagesDatabaseReferences.
                                                        orderByChild("text").equalTo("DisplayPhoto");
                                                applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                            String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                                            Picasso.with(getApplicationContext()).load
                                                                    (value).fit().centerCrop().into(ava);
                                                        }
                                                        askLocLayout.setVisibility(View.VISIBLE);
                                                    }
                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                    }
                                                });
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    final String value = appleSnapshot.getValue().toString().replace("text=Allow, name=", "").replace("{", "").replace("}", "");
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(value.replaceAll(" ", "") + " Location");
                                    mChildEventListener12345 = new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(value.replaceAll(" ", "") + " Location");
                                            Query applesQuery1334 = mMessagesDatabaseReferences.
                                                    orderByChild("text").equalTo("MyLoc");
                                            applesQuery1334.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(value.replaceAll(" ", "") + " Location");
                                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                        String value1 = appleSnapshot.getValue().toString().replace("text=MyLoc, name=", "")
                                                                .replace("{", "").replace("}", "");
                                                        final int koma2 = value1.indexOf(",");
                                                        double Longtitude = Double.parseDouble(value1.substring(0, koma2).replaceAll("[^0-9], .", "").replace("{", ""));
                                                        double Latitude = Double.parseDouble(value1.substring(koma2 + 1).replaceAll("[^0-9], .", "").replace("{", "").replace("}", ""));
                                                        LatLng position = new LatLng(Latitude, Longtitude);
                                                        if (mapReady) {
                                                            m_map.clear();
                                                            m_map.addMarker(new MarkerOptions().position(position)
                                                                    .title(askLocName.getText().toString() + " position"));
                                                            m_map.moveCamera(CameraUpdateFactory.newLatLng(position));
                                                        }
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                                        }
                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                        }
                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                                            final int koma2 = value.indexOf(",");
                                            dataSnapshot.getRef().removeValue();
                                            if (mapReady) {
                                                m_map.clear();
                                            }
                                        }
                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    };
                                    mMessagesDatabaseReferences.addChildEventListener(mChildEventListener12345);
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }
                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };
                mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
            }
            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
            }
            mChildEventListener7777 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String value123 = dataSnapshot.getValue().toString().replace("{text=Stop, name=", "").replace("}", "");
                    if (value123.equals("it!")) {
                        askLocLayout.setVisibility(View.GONE);
                        refuseButton.setVisibility(View.VISIBLE);
                        allowButton.setText("Allow");
                        dataSnapshot.getRef().removeValue();
                    }
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    String value = dataSnapshot.getValue().toString().replace("{text=", "");
                    int koma2 = value.indexOf(",");
                    if (value.substring(koma2 + 2).equals("Allow")) {
                        askLocLayout.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener7777);
            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
            if (FirebaseAuth.getInstance().getCurrentUser() != null)
                userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
            else
                userNumber = "anon";
            mChildEventListener123 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                    Query applesQuery1 = mMessagesDatabaseReferences.
                            orderByChild("PhoneNumber").equalTo(userNumber.replaceAll("[^0-9]", ""));
                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                }
                                Query applesQuery1 = mMessagesDatabaseReferences.
                                        orderByChild("text").equalTo("Share loc");
                                applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                        }
                                        int klmn = 0;
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            klmn++;
                                        }
                                        if (klmn == 1) {
                                            //Somebody want to share currentLocation with U
                                            String data = appleSnapshot.getValue().toString();
                                            int slash = data.indexOf("/");
                                            int mes = data.indexOf("message=");
                                            String name = data.substring(0, slash).substring(mes).replace("message=", "");
                                            String number0 = data.substring(slash);
                                            String number = number0.substring(0, number0.indexOf(",")).replaceAll("/", "");
                                            //ll
                                            askLocLiketoAsk.setText("Want to share currentLocation with you");
                                            askLocName.setText(name);
                                            askLocNumber.setText(number);
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(askLocNumber.getText().toString());
                                            Query applesQuery99 = mMessagesDatabaseReferences.
                                                    orderByChild("text").equalTo("DisplayPhoto");
                                            applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                        String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                                        Picasso.with(getApplicationContext()).load
                                                                (value).fit().centerCrop().into(ava);
                                                    }
                                                    askLocLayout.setVisibility(View.VISIBLE);
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                                        } else if (klmn == 0) {
                                            //Somebody asks for you loc
                                            String data = appleSnapshot.getValue().toString();
                                            int slash = data.indexOf("/");
                                            int mes = data.indexOf("message=");
                                            String name = data.substring(0, slash).substring(mes).replace("message=", "");
                                            String number0 = data.substring(slash).replaceAll("/", "").replace("}", "");
                                            if(number0.length() > 9){
                                                number0 = number0.substring(0, number0.indexOf(","));
                                            }
                                            //ll
                                            askLocLiketoAsk.setText("Ask for your currentLocation");
                                            askLocName.setText(name);
                                            askLocNumber.setText(number0);
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(askLocNumber.getText().toString());
                                            Query applesQuery99 = mMessagesDatabaseReferences.
                                                    orderByChild("text").equalTo("DisplayPhoto");
                                            applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                        String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                                        Picasso.with(getApplicationContext()).load
                                                                (value).fit().centerCrop().into(ava);
                                                    }
                                                    askLocLayout.setVisibility(View.VISIBLE);
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                            }
                            }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    askLocLayout.setVisibility(View.GONE);
                    refuseButton.setVisibility(View.VISIBLE);
                    allowButton.setText("Allow");
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
                                /*
                                final String value = appleSnapshot.getValue().toString().replace("PhoneNumber=", "").
                                        replace("timestamp=","").replace("{", "").
                                        replace("}", "").replace("message=", "");
                                final int koma2 = value.indexOf(",");
                                final int slash = value.indexOf("/");
                                String value1 = value.substring(koma2).replace(", ", "");
                                final String value2;
                                int slash1 = value1.indexOf("/");
                                if(!value1.contains("whould like to share currentLocation with you")) {
                                    int b = value1.indexOf("b");
                                    value2 = value1.substring(b).replace("/","").replaceAll("[^0-9]", "");
                                    Toast.makeText(getApplicationContext(), value2, Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    int b = value1.indexOf("b");
                                    value2 = value1.substring(b).replaceAll("[^0-9]", "");
                                }
                                int msg = dataSnapshot.getValue().toString().indexOf("message");
                                int tmst = dataSnapshot.getValue().toString().indexOf("timestamp");
                                Query applesQuery123 = mMessagesDatabaseReferences.orderByChild("text").equalTo("DisplayName");
                                applesQuery123.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(value.substring(0, koma2));
                                        for (DataSnapshot appleSnapshot123 : dataSnapshot.getChildren()) {
                                            String name = appleSnapshot123.getValue().toString().replace("{text=DisplayName, name=", "").replace("}", "");
                                            askLocName.setText(name);
                                            askLocNumber.setText("12");
                                            askLocLayout.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    int koma = dataSnapshot.getValue().toString().indexOf(",");
                    String value2 = dataSnapshot.getValue().toString().substring(koma).replace(", ", "")
                            .substring(koma).replace(", ", "").replace("PhoneNumber=", "").replace("{", "").replace("}", "").replace("e currentLocation with you", "");
                    if (value2.equals(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3))){
                        refuseButton.setVisibility(View.VISIBLE);
                        allowButton.setText("Allow");
                        askLocLayout.setVisibility(View.GONE);
                        Query applesQuery99 = mMessagesDatabaseReferences.
                                orderByChild("text").equalTo("Share loc");
                        applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                    DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(getApplicationContext());
                    String number = dataSnapshot.getValue().toString().replace("{message=" + mDictionary.getContact(1)
                            .getName() + "/" + FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3), "")
                            .replace(", body=Asking for Your Location, PhoneNumber=", "").replace(
                                    askLocNumber.getText().toString(), "").replace("}", "").replaceAll(" ", "");
                    String value = dataSnapshot.getValue().toString().replace("{message=" + mDictionary.getContact(1)
                            .getName() + "/", "").replace(", body=Asking for Your Location, PhoneNumber=", "").replace(
                            askLocNumber.getText().toString().replaceAll(" ", ""), "").replace("}", "").replace(number, "");
                    if (value.equals(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3))){
                        refuseButton.setVisibility(View.VISIBLE);
                        allowButton.setText("Allow");
                        askLocLayout.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
            }
            mChildEventListener123 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Query applesQuery1 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Share loc");
                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                            }
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                final String value = appleSnapshot.getValue().toString();
                                final int koma = value.indexOf(",");
                                askLocNumber.setText(value.substring(koma + 2).replace("name=", "").replace("}", ""));
                                askLocLiketoAsk.setText("whould like to share currentLocation with you");
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                        .child(value.substring(koma + 2).replace("name=", "").replace("}", ""));
                                Query applesQuery1 = mMessagesDatabaseReferences.
                                        orderByChild("text").equalTo("DisplayName");
                                applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(value.substring(koma + 2).replace("name=", "").replace("}", ""));
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            askLocName.setText(appleSnapshot.getValue().toString()
                                                    .replace("{text=DisplayName, name=", "").replace("}", ""));
                                            askLocLayout.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
            */
            allowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                    }
                    Query applesQuery1 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Share loc");
                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                            }
                            int klmn = 0;
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                klmn++;
                            }
                            if (klmn == 1) {
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", ""));
                                Message message33 = new Message("number", FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                mMessagesDatabaseReferences.push().setValue(message33);
                                if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                }
                                if(allowButton.getText().toString().equals("Allow")) {
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        Message message = new Message("Allow share", FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", "") + " Location");
                                        mMessagesDatabaseReferences.push().setValue(message);
                                    }
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3).replaceAll(" ", ""));
                                        Message message1 = new Message("Allow", askLocNumber.getText().toString().replaceAll(" ", ""));
                                        mMessagesDatabaseReferences.push().setValue(message1);
                                    }
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(askLocNumber.getText().toString());
                                    Query applesQuery99 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("DisplayPhoto");
                                    applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                                Picasso.with(getApplicationContext()).load
                                                        (value).fit().centerCrop().into(ava);
                                            }
                                            askLocLayout.setVisibility(View.VISIBLE);
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    refuseButton.setVisibility(View.GONE);
                                    allowButton.setText("Stop Recieving");
                                }
                                else if (allowButton.getText().toString().equals("Stop Sharing")) {
//oooo
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    Query applesQuery1 = mMessagesDatabaseReferences.
                                            orderByChild("PhoneNumber").equalTo(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (final DataSnapshot appleSnapshot2 : dataSnapshot.getChildren()) {
                                                appleSnapshot2.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                        userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    Query applesQuery19 = mMessagesDatabaseReferences.
                                            orderByChild("PhoneNumber").equalTo(askLocNumber.getText().toString().replaceAll(" ", ""));
                                    applesQuery19.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            int klmn = 0;
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                                klmn++;
                                            }
                                            if(klmn == 0){
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    Query applesQuery12 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Share loc");
                                    applesQuery12.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(askLocNumber.getText().toString().replaceAll(" ", ""));
                                    Query applesQuery678 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow");
                                    applesQuery678.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    Query applesQuery123 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Share loc");
                                    applesQuery123.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                        Query applesQuery1999 = mMessagesDatabaseReferences.
                                                orderByChild("text").equalTo("Allow");
                                        applesQuery1999.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                    appleSnapshot.getRef().removeValue();
                                                }
                                            }
                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                    }
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(askLocNumber.getText().toString());
                                    Query applesQuery199 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("number");
                                    applesQuery199.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", "") + " Location");
                                    mMessagesDatabaseReferences.removeValue();
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", ""));
                                    Query applesQuery1999 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow");
                                    applesQuery1999.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", ""));
                                }
                                else if(allowButton.getText().toString().equals("Stop Recieving")){
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                        userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    Query applesQuery1 = mMessagesDatabaseReferences.
                                            orderByChild("PhoneNumber").equalTo(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ",""));
                                    Message message99 = new Message("Stop", "it!");
                                    mMessagesDatabaseReferences.push().setValue(message99);
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                        Query applesQuery1999 = mMessagesDatabaseReferences.
                                                orderByChild("text").equalTo("Allow");
                                        applesQuery1999.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                    appleSnapshot.getRef().removeValue();
                                                }
                                            }
                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                    }
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(askLocNumber.getText().toString());
                                    Query applesQuery199 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("number");
                                    applesQuery199.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    }
                                    Query applesQuery177 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Share loc");
                                    applesQuery177.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                                allowButton.setText("Allow");
                                                askLocLiketoAsk.setText("whould like to get your currentLocation");
                                                refuseButton.setVisibility(View.VISIBLE);
                                                askLocLayout.setVisibility(View.GONE);
                                                m_map.clear();
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", "") + " Location");
                                                mMessagesDatabaseReferences.removeValue();
                                                m_map.clear();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                                    mMessagesDatabaseReferences.removeValue();
                                    String askLocNumber2 = askLocNumber.getText().toString()
                                            .replace("#", "").replace(".","").replace("$", "");
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(askLocNumber2.replaceAll(" ", "") + " Location");
                                    mMessagesDatabaseReferences.removeValue();
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber2.replaceAll(" ", ""));
                                    Message message1 = new Message("Stop", "it!");
                                    mMessagesDatabaseReferences.push().setValue(message1);
                                }
                            } else if (klmn == 0) {
                                if (allowButton.getText().toString().equals("Allow")) {
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                                    }
                                    Message message = new Message("Allowed", "");
                                    mMessagesDatabaseReferences.push().setValue(message);
                                    GPSTrackerHelper gpsTrackerHelper = new GPSTrackerHelper(getApplicationContext(), "WIFI");
                                    Message message1 = new Message("MyLoc", gpsTrackerHelper.latitude + "," + gpsTrackerHelper.longitude);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", ""));
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        Message message123 = new Message("Allow", FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                        mMessagesDatabaseReferences.push().setValue(message123);
                                    }
                                    askLocLiketoAsk.setText("sharing currentLocation with");
                                    allowButton.setText("Stop Sharing");
                                    refuseButton.setVisibility(View.GONE);
                                } else if (allowButton.getText().equals("Stop Sharing")) {
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    Query applesQuery1 = mMessagesDatabaseReferences.
                                            orderByChild("PhoneNumber").equalTo(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (final DataSnapshot appleSnapshot2 : dataSnapshot.getChildren()) {
                                                appleSnapshot2.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                        userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    Query applesQuery15 = mMessagesDatabaseReferences.
                                            orderByChild("PhoneNumber").equalTo(askLocNumber.getText().toString().replaceAll("[^0-9]", ""));
                                    applesQuery15.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ",""));
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", ""));
                                    Query applesQuery1999 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow");
                                    applesQuery1999.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                                    }
                                    allowButton.setText("Allow");
                                    refuseButton.setVisibility(View.VISIBLE);
                                    askLocLayout.setVisibility(View.GONE);
                                    mMessagesDatabaseReferences.removeValue();
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    }
                                    Query applesQuery99 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("number");
                                    applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                            }
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(askLocNumber.getText().toString().replaceAll(" ", ""));
                                    Query applesQuery999 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Share loc");
                                    applesQuery999.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(askLocNumber.getText().toString());
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString());
                                    Query applesQuery155 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow");
                                    applesQuery155.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString());
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                                askLocLayout.setVisibility(View.GONE);
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                                        mMessagesDatabaseReferences.removeValue();
                                    }
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    }
                                    Query applesQuery123 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Share loc");
                                    applesQuery123.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                            }
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                                askLocLayout.setVisibility(View.GONE);
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    }
                                    Query applesQuery12 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow share");
                                    applesQuery12.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    Message message99 = new Message("Stop", "it!");
                                    mMessagesDatabaseReferences.push().setValue(message99);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                                    mMessagesDatabaseReferences.removeValue();
                                    String askLocNumber2 = askLocNumber.getText().toString()
                                            .replace("#", "").replace(".","").replace("$", "");
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(askLocNumber2.replaceAll(" ", "") + " Location");
                                    mMessagesDatabaseReferences.removeValue();
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ",""));
                                    Message message999 = new Message("Stop", "it!");
                                    mMessagesDatabaseReferences.push().setValue(message999);
                                } else if (allowButton.getText().toString().equals("Stop Recieving")) {
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                        userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                    Query applesQuery1 = mMessagesDatabaseReferences.
                                            orderByChild("PhoneNumber").equalTo(askLocNumber.getText().toString().replaceAll("[^0-9]", ""));
                                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    allowButton.setText("Allow");
                                    refuseButton.setVisibility(View.VISIBLE);
                                    askLocLayout.setVisibility(View.GONE);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ",""));
                                    Message message99 = new Message("Stop", "it!");
                                    mMessagesDatabaseReferences.push().setValue(message99);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    Query applesQuery12 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Share loc");
                                    applesQuery12.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                                askLocLayout.setVisibility(View.GONE);
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    Query applesQuery1234 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow");
                                    applesQuery1234.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                                askLocLayout.setVisibility(View.GONE);
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString());
                                    mChildEventListener123 = new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        }
                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                        }
                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.getValue().toString().replace("{text=Share loc, name=", "").replace("}", "").equals("")) {
                                                allowButton.setText("Allow");
                                                refuseButton.setVisibility(View.VISIBLE);
                                                askLocLayout.setVisibility(View.GONE);
                                            }
                                        }
                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    };
                                    mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().
                                            child(askLocNumber.getText().toString().replaceAll(" ", "") + " Location");
                                    mMessagesDatabaseReferences.removeValue();
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", ""));
                                    Message message999 = new Message("Stop", "it!");
                                    mMessagesDatabaseReferences.push().setValue(message999);
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    Query applesQuery123 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow share");
                                    applesQuery123.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                appleSnapshot.getRef().removeValue();
                                                askLocLayout.setVisibility(View.GONE);
                                            }
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString());
                                            Query applesQuery1 = mMessagesDatabaseReferences.
                                                    orderByChild("text").equalTo("Share loc");
                                            applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString());
                                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                        appleSnapshot.getRef().removeValue();
                                                        askLocLayout.setVisibility(View.GONE);
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().
                                            child(askLocNumber.getText().toString().replaceAll(" ", "") + " Location");
                                    mMessagesDatabaseReferences.removeValue();
                                    if(mapReady){
                                        m_map.clear();
                                    }
                                }
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            });
            final LinearLayout llBottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet1);
            // init the bottom sheet behavior
            final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
            // change the state of the bottom sheet
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//go
            // set the peek height
            bottomSheetBehavior.setPeekHeight(310);
            // set hideable or not
            bottomSheetBehavior.setHideable(false);

            // set callback for changes
            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                }
                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            });
            final Button shareLocButton = (Button)findViewById(R.id.shareLocButton);
            shareLocButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (cm.getActiveNetworkInfo() != null) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        if (prefs.getString("key", null).equals("")) {
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            editor.putString("key", "Pressed"); //InputString: from the EditText
                            editor.commit();
                            shareLocButton.setBackgroundColor(Color.parseColor("#817D7D"));
                        } else {
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            editor = prefs.edit();
                            editor.putString("key", ""); //InputString: from the EditText
                            editor.commit();
                            shareLocButton.setBackgroundColor(Color.parseColor("#000000"));
                        }
                    }
                    else {
                        Toast toast = Toast.makeText(getApplicationContext(), "No network connection", Toast.LENGTH_SHORT);
                        if (toast == null || !toast.getView().isShown()) {
                            toast.show();
                        }
                    }
                }
            });
            inAppmMessageAdapter = new MessageAdapter(messagesInApp);
            inAppmMessageListView = (RecyclerView) findViewById(R.id.inAppContactsListView);
            RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getApplicationContext());
            inAppmMessageListView.setLayoutManager(mLayoutManager2);
            final TextView outsideAppMenu = (TextView)findViewById(R.id.outsideAppMenu);
            final TextView insideAppMenu = (TextView)findViewById(R.id.insideAppMenu);
            final TextView allContMenu = (TextView)findViewById(R.id.allContMenu);
            outsideAppMenu.setVisibility(View.VISIBLE);
            insideAppMenu.setVisibility(View.VISIBLE);
            outsideAppmMessageAdapter = new MessageAdapter(outsideAnApp);
            outsideAppmMessageListView = (RecyclerView) findViewById(R.id.outsideAnAppContactsListView);
            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getApplicationContext());
            outsideAppmMessageListView.setLayoutManager(mLayoutManager3);
            inAppmMessageAdapter2 = new MessageAdapter(messagesInApp2);
            outsideAppmMessageAdapter2 = new MessageAdapter(outsideAnApp2);
            final TextView inAppTxt = (TextView)findViewById(R.id.inAppTxt);
            final TextView outsideAppTxt = (TextView)findViewById(R.id.outsideAppTxt);
            favoritesListView = (RecyclerView)findViewById(R.id.favoritesListView);
            final TextView favoritesTxt = (TextView)findViewById(R.id.favoritesTxt);
            favoritesMessageAdapter = new MessageAdapter(favoritesArray);
            favoritesMessageAdapter2 = new MessageAdapter(favoritesArray2);
            RecyclerView.LayoutManager mLayoutManager4 = new LinearLayoutManager(getApplicationContext());
            favoritesListView.setLayoutManager(mLayoutManager4);

            DBHelper dbHelper = new DBHelper(getApplicationContext());
            if(!dbHelper.getAllContacts().isEmpty()){


                Message message = new Message(dbHelper.getAllContacts().get(0).getText(), dbHelper.getAllContacts2().get(0).getName());
                favoritesArray.add(message);
                favoritesTxt.setVisibility(View.VISIBLE);
                inAppTxt.setVisibility(View.VISIBLE);
                favoritesListView.setAdapter(favoritesMessageAdapter);
                favoritesListView.setVisibility(View.VISIBLE);

            }

            /*


            if(FirebaseAuth.getInstance().getCurrentUser() != null)


            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance()
                    .getCurrentUser().getPhoneNumber().substring(3) + " favorites");
            mChildEventListener123 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String currentText = searchEdit.getText().toString();
                    searchEdit.setText("");
                    searchEdit.setText(currentText);
                    searchEdit.setSelection(searchEdit.getText().length());
                    int koma = dataSnapshot.getValue().toString().indexOf(",");
                    String name = dataSnapshot.getValue().toString().substring(0, koma).replace("{text=","");
                    String number = dataSnapshot.getValue().toString().substring(koma + 2).replace("name=","").replace("}", "");
                    Message message1 = new Message(name, number);
                    if(favoritesListView.getAdapter() == favoritesMessageAdapter) {
                        if(searchEdit.getText().toString().trim().equals("Search contacts") || searchEdit.getText().toString().trim().equals("")) {
                                favoritesArray.add(0, message1);
                                favoritesListView.setAdapter(favoritesMessageAdapter);
                        }
                        else if (name.contains(searchEdit.getText().toString())) {
                            favoritesArray.add(0, message1);
                            favoritesListView.setAdapter(favoritesMessageAdapter);
                            favoritesArray2.add(0, message1);
                        }
                    }
                    else if (favoritesListView.getAdapter() == favoritesMessageAdapter2){
                        if(searchEdit.getText().toString().trim().equals("Search contacts") ) {
                            favoritesArray2.add(0, message1);
                            favoritesListView.setAdapter(favoritesMessageAdapter2);
                        }
                        else if (name.contains(searchEdit.getText().toString())) {
                            favoritesArray.add(0, message1);
                            favoritesListView.setAdapter(favoritesMessageAdapter2);
                            favoritesArray2.add(0, message1);
                        }
                    }
                    else {
                        if(searchEdit.getText().toString().trim().equals("Search contacts")) {
                            favoritesArray.add(0, message1);
                            favoritesListView.setAdapter(favoritesMessageAdapter);
                        }
                        else if (name.contains(searchEdit.getText().toString())) {
                            favoritesArray.add(0, message1);
                            favoritesListView.setAdapter(favoritesMessageAdapter);
                            favoritesArray2.add(0, message1);
                        }
                    }
                    favoritesListView.setVisibility(View.VISIBLE);
                    favoritesTxt.setVisibility(View.VISIBLE);
                    if(inAppmMessageListView.getVisibility() == View.VISIBLE)
                        inAppTxt.setVisibility(View.VISIBLE);
                    if(outsideAppmMessageListView.getVisibility() == View.VISIBLE)
                        outsideAppTxt.setVisibility(View.VISIBLE);
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    String currentText = searchEdit.getText().toString();
                    searchEdit.setText("");
                    searchEdit.setText(currentText);
                    searchEdit.setSelection(searchEdit.getText().length());
                    if(searchEdit.getText().toString().equals("Search contacts") || searchEdit.getText().toString().trim().equals("")) {
                        if (favoritesListView.getAdapter() == favoritesMessageAdapter) {
                            int count;
                            int koma = dataSnapshot.getValue().toString().indexOf(",");
                            String number = dataSnapshot.getValue().toString().substring(koma + 2).replace("name=", "").replace("}", "");
                            String name = dataSnapshot.getValue().toString().substring(0, koma).replace("{text=", "");
                            for (count = 0; count < favoritesArray.size(); count++) {
                                if (!favoritesArray.get(count).getText().equals(name) )  {
                                    favoritesArray2.add(favoritesArray.get(count));
                                }
                            }
                            favoritesListView.setAdapter(favoritesMessageAdapter2);
//tttttttttttttttttt
                            favoritesArray.clear();
                            koma = dataSnapshot.getValue().toString().indexOf(",");
                            number = dataSnapshot.getValue().toString().substring(koma + 2).replace("name=", "").replace("}", "");
                            name = dataSnapshot.getValue().toString().substring(0, koma).replace("{text=", "");
                            for (count = 0; count < favoritesArray2.size(); count++) {
                                if (!favoritesArray2.get(count).getText().equals(name) ) {
                                    favoritesArray.add(favoritesArray2.get(count));
                                }
                            }
                            if (favoritesArray2.isEmpty())
                                favoritesTxt.setVisibility(View.GONE);
                        }
                        else if (favoritesListView.getAdapter() == favoritesMessageAdapter2) {
                            favoritesArray.clear();
                            int count;
                            int koma = dataSnapshot.getValue().toString().indexOf(",");
                            String number = dataSnapshot.getValue().toString().substring(koma + 2).replace("name=", "").replace("}", "");
                            String name = dataSnapshot.getValue().toString().substring(0, koma).replace("{text=", "");
                            for (count = 0; count < favoritesArray2.size(); count++) {
                                if (!favoritesArray2.get(count).getText().equals(name) ) {
                                    favoritesArray.add(favoritesArray2.get(count));
                                }
                            }
                            favoritesArray2.clear();
                            favoritesListView.setAdapter(favoritesMessageAdapter);
                            if (favoritesArray.isEmpty())
                                favoritesTxt.setVisibility(View.GONE);
                        }
                    }
                    else{
                        setTextq("go");
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance()
                                .getCurrentUser().getPhoneNumber().substring(3) + " favorites");
                        mChildEventListener123 = new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                int koma = dataSnapshot.getValue().toString().indexOf(",");
                                String number = dataSnapshot.getValue().toString().substring(koma + 2).replace("name=", "").replace("}", "");
                                String name = dataSnapshot.getValue().toString().substring(0, koma).replace("{text=", "");
                                addNewItemArray(number, name);
                            }
                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            }
                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {
                            }
                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        };
                        mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
                        int koma = dataSnapshot.getValue().toString().indexOf(",");
                        String number = dataSnapshot.getValue().toString().substring(koma + 2).replace("name=", "").replace("}", "");
                        String name = dataSnapshot.getValue().toString().substring(0, koma).replace("{text=", "");
                        if (favoritesListView.getAdapter() == favoritesMessageAdapter) {
                            int count;
                            favoritesArray2.clear();
                            for (count = 0; count < favoritesArray.size(); count++) {
                                if (!favoritesArray.get(count).getText().equals(name)) {
                                    favoritesArray2.add(favoritesArray.get(count));
                                }
                            }
                            favoritesListView.setAdapter(favoritesMessageAdapter2);
                            if (favoritesArray2.isEmpty())
                                favoritesTxt.setVisibility(View.GONE);
                        }
                        else if (favoritesListView.getAdapter() == favoritesMessageAdapter2) {
                            int count;
                            favoritesArray.clear();
                            for (count = 0; count < favoritesArray2.size(); count++) {
                                if (!favoritesArray2.get(count).getText().equals(name)&&
                                        favoritesArray2.get(count).getText().contains(searchEdit.getText().toString())) {
                                    favoritesArray.add(favoritesArray2.get(count));
                                }
                            }
                            favoritesListView.setAdapter(favoritesMessageAdapter);
                            if (favoritesArray.isEmpty())
                                favoritesTxt.setVisibility(View.GONE);
                           mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                   .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber() + " favorites");
                            mChildEventListener123 = new ChildEventListener() {
                                @Override
                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                    int koma = dataSnapshot.getValue().toString().indexOf(",");
                                    String name = dataSnapshot.getValue().toString().substring(0, koma).replace("{text=", "");
                                    String number = dataSnapshot.getValue().toString().substring(koma + 2)
                                            .replace("name=","").replace("}", "");
                                    addNewItemArray(name, number);
                                }
                                @Override
                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                }
                                @Override
                                public void onChildRemoved(DataSnapshot dataSnapshot) {
                                }
                                @Override
                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            };
                            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
                        }
                    }
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);

            */

            displayContacts();
            final ColorStateList color = insideAppMenu.getTextColors();
            insideAppMenu.setTextColor(Color.parseColor("#000000"));
            outsideAppmMessageListView.setVisibility(View.GONE);
            inAppmMessageListView.setVisibility(View.VISIBLE);
            outsideAppMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inAppTxt.setVisibility(View.GONE);
                    if(favoritesArray.isEmpty()) {
                        outsideAppTxt.setVisibility(View.GONE);
                    }
                    else
                        outsideAppTxt.setVisibility(View.VISIBLE);
                    outsideAppMenu.setTextColor(Color.parseColor("#000000"));
                    insideAppMenu.setTextColor(color);
                    allContMenu.setTextColor(color);
                    inAppmMessageListView.setVisibility(View.GONE);
                    outsideAppmMessageListView.setVisibility(View.VISIBLE);
                    searchEdit.setText(searchEdit.getText() + "");
                    searchEdit.setSelection(searchEdit.getText().length());
                }
            });
            allContMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inAppTxt.setVisibility(View.VISIBLE);
                    outsideAppTxt.setVisibility(View.VISIBLE);
                    outsideAppMenu.setTextColor(color);
                    insideAppMenu.setTextColor(color);
                    allContMenu.setTextColor(Color.parseColor("#000000"));
                    inAppmMessageListView.setVisibility(View.VISIBLE);
                    outsideAppmMessageListView.setVisibility(View.VISIBLE);
                    searchEdit.setText(searchEdit.getText() + "");
                    searchEdit.setSelection(searchEdit.getText().length());
                }
            });
            insideAppMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(favoritesArray.isEmpty()) {
                        inAppTxt.setVisibility(View.GONE);
                    }
                    else
                        inAppTxt.setVisibility(View.VISIBLE);
                    outsideAppTxt.setVisibility(View.GONE);
                    outsideAppMenu.setTextColor(color);
                    insideAppMenu.setTextColor(Color.parseColor("#000000"));
                    allContMenu.setTextColor(color);
                    inAppmMessageListView.setVisibility(View.VISIBLE);
                    outsideAppmMessageListView.setVisibility(View.GONE);
                    searchEdit.setText(searchEdit.getText() + "");
                    searchEdit.setSelection(searchEdit.getText().length());
                }
            });
            MessageAdapter currentAdapter = null;
            MessageAdapter currentAdapter2 = null;
            List<Message> currentArray = null;
            List<Message> currentArray2 = null;
            if(favoritesListView.getAdapter() == favoritesMessageAdapter) {
                currentAdapter = favoritesMessageAdapter;
                currentAdapter2 = favoritesMessageAdapter2;
                currentArray = favoritesArray;
                currentArray2 = favoritesArray2;
            }
            else if(favoritesListView.getAdapter() == favoritesMessageAdapter2){
                currentAdapter = favoritesMessageAdapter2;
                currentAdapter2 = favoritesMessageAdapter;
                currentArray = favoritesArray2;
                currentArray2 = favoritesArray;
            }
            else {
                currentAdapter = favoritesMessageAdapter;
                currentAdapter2 = favoritesMessageAdapter2;
                currentArray = favoritesArray;
                currentArray2 = favoritesArray2;
            }
            final List<Message> finalCurrentArray = currentArray2;
            final MessageAdapter finalCurrentAdapter = currentAdapter;
            final List<Message> finalCurrentArray1 = currentArray;
            final MessageAdapter finalCurrentAdapter1 = currentAdapter2;
            final List<Message> finalCurrentArray2 = currentArray;
            final MessageAdapter finalCurrentAdapter2 = currentAdapter;
            final List<Message> finalCurrentArray3 = currentArray2;
            final MessageAdapter finalCurrentAdapter3 = currentAdapter;
            searchEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    RecyclerView allContsListView = (RecyclerView)findViewById(R.id.allContactsListView);
                    if(allContsListView.getVisibility() == View.VISIBLE){
                        MessageAdapter allContactsAdapter2 = new MessageAdapter(allConts2);
                        MessageAdapter allContactsAdapter = new MessageAdapter(allConts);
                        allConts2.clear();
                        String text = searchEdit.getText().toString();
                        if (text.equals("")) {
                            allConts2.clear();
                            allContsListView.setAdapter(allContactsAdapter);
                        } else {
                            for (count = 0; count < allConts.size(); count++) {
                                if (allConts.get(count).getText().contains(text)) {
                                    allConts2.add(allConts.get(count));
                                }
                            }
                            allContsListView.setAdapter(allContactsAdapter2);
                        }
                    }
                    else if (inAppmMessageListView.getVisibility() == View.VISIBLE
                            && outsideAppmMessageListView.getVisibility() == View.VISIBLE &&
                            favoritesListView.getVisibility() == View.VISIBLE) {
                        messagesInApp2.clear();
                        outsideAnApp2.clear();
                        String text = searchEdit.getText().toString();
                        if (text.equals("")) {
                            if(!getTextq().equals("")){
                                favoritesArray.clear();
                                for(int position=0; position<newItems.size(); position++) {
                                    String name = getNewItemName(position);
                                    String number = getNewItemNumber(position);
                                    Message message1 = new Message(name, number);
                                    favoritesArray.add(message1);
                                    favoritesMessageAdapter.notifyDataSetChanged();
                                }
                            }
                            setTextq("");
                            newItems.clear();
                            favoritesListView.setAdapter(finalCurrentAdapter2);
                            inAppmMessageListView.setAdapter(inAppmMessageAdapter);
                            outsideAppmMessageListView.setAdapter(outsideAppmMessageAdapter);
                        } else {
                            for (count = 0; count < messagesInApp.size(); count++) {
                                if (messagesInApp.get(count).getText().contains(text)) {
                                    messagesInApp2.add(messagesInApp.get(count));
                                }
                            }
                            for (count = 0; count < outsideAnApp.size(); count++) {
                                if (outsideAnApp.get(count).getText().contains(text)) {
                                    outsideAnApp2.add(outsideAnApp.get(count));
                                }
                            }
                            for (count = 0; count < finalCurrentArray1.size(); count++) {
                                if (finalCurrentArray1.get(count).getText().contains(text)) {
                                    favoritesArray2.add(finalCurrentArray1.get(count));
                                }
                            }
                            favoritesListView.setAdapter(finalCurrentAdapter1);
                            inAppmMessageListView.setAdapter(inAppmMessageAdapter2);
                            outsideAppmMessageListView.setAdapter(outsideAppmMessageAdapter2);
                        }
                    }
                    else if (inAppmMessageListView.getVisibility() == View.VISIBLE) {
                        messagesInApp2.clear();
                        if(!getTextq().equals("")){
                            favoritesArray.clear();
                            for(int position=0; position<newItems.size(); position++) {
                                String name = getNewItemName(position);
                                String number = getNewItemNumber(position);
                                Message message1 = new Message(name, number);
                                favoritesArray.add(message1);
                                favoritesMessageAdapter.notifyDataSetChanged();
                            }
                        }
                        setTextq("");
                        newItems.clear();
                        String text = searchEdit.getText().toString();
                        if (text.equals("")) {
                            inAppmMessageListView.setAdapter(inAppmMessageAdapter);
                            favoritesListView.setAdapter(finalCurrentAdapter);
                            favoritesListView.setVisibility(View.VISIBLE);
                        } else {
                            for (count = 0; count < messagesInApp.size(); count++) {
                                if (messagesInApp.get(count).getText().contains(text)) {
                                    messagesInApp2.add(messagesInApp.get(count));
                                    inAppmMessageAdapter.notifyDataSetChanged();
                                }
                            }
                            inAppmMessageListView.setAdapter(inAppmMessageAdapter2);
                        }
                    }
                    else if (outsideAppmMessageListView.getVisibility() == View.VISIBLE) {
                        outsideAnApp2.clear();
                        String text = searchEdit.getText().toString();
                        if (text.equals("")) {
                            if(!getTextq().equals("")){
                                favoritesArray.clear();
                                for(int position=0; position<newItems.size(); position++) {
                                    String name = getNewItemName(position);
                                    String number = getNewItemNumber(position);
                                    Message message1 = new Message(name, number);
                                    favoritesArray.add(message1);
                                    favoritesMessageAdapter.notifyDataSetChanged();
                                }
                            }
                            setTextq("");
                            newItems.clear();
                            outsideAppmMessageListView.setAdapter(outsideAppmMessageAdapter);
                        } else {
                            for (count = 0; count < outsideAnApp.size(); count++) {
                                if (outsideAnApp.get(count).getText().contains(text)) {
                                    outsideAnApp2.add(outsideAnApp.get(count));
                                    outsideAppmMessageAdapter.notifyDataSetChanged();
                                }
                            }
                            outsideAppmMessageListView.setAdapter(outsideAppmMessageAdapter2);
                        }
                    }
                    if (favoritesListView.getVisibility() == View.VISIBLE) {
                        finalCurrentArray.clear();
                        String text = searchEdit.getText().toString();
                        if (text.equals("")) {
                            favoritesListView.setAdapter(finalCurrentAdapter);
                        } else {
                            for (count = 0; count < finalCurrentArray1.size(); count++) {
                                if (finalCurrentArray1.get(count).getText().contains(text)) {
                                    finalCurrentArray.add(finalCurrentArray1.get(count));
                                    finalCurrentAdapter.notifyDataSetChanged();
                                }
                            }
                            favoritesListView.setAdapter(finalCurrentAdapter1);
                        }
                    }
                }
                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                String userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
            }
            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
            mChildEventListener123 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    int koma = dataSnapshot.getValue().toString().indexOf(",");
                    //tuuut
                    if(dataSnapshot.getValue().toString().replace("{", "").replace("}", "").substring(koma + 2).replace("ame=", "").replace("text=number, ", "").equals(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3))){
                        askLocLayout.setVisibility(View.GONE);
                        allowButton.setText("Allow");
                        refuseButton.setVisibility(View.VISIBLE);
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                        Query applesQuery99 = mMessagesDatabaseReferences.
                                orderByChild("text").equalTo("Allow");
                        applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
            mMessagesDatabaseReferences = mFirebaseDatabase.getInstance().getReference().child(userNumber + " favorites");
            MessageAdapter messageAdapter123 = new MessageAdapter(null);
            String itemText1 = messageAdapter123.getTexxxt();
            Query applesQuery99 = mMessagesDatabaseReferences.
                    orderByChild("text").equalTo(itemText1);
            applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mMessagesDatabaseReferences = mFirebaseDatabase.getInstance().getReference().child(userNumber + " favorites");
                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                        setSnapshot(appleSnapshot);
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            if (FirebaseAuth.getInstance().getCurrentUser() != null)
                userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(userNumber + " favorites");
            mChildEventListener1 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Message message = dataSnapshot.getValue(Message.class);
                    messages.add(0, message);
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    String value = dataSnapshot.getValue().toString().replace("text=", "").replace("name=", "").replace("{", "").replace("}", "");
                    String koma = ",";
                    int koma2 = value.indexOf(",");
                    String name = value.substring(0, koma2);
                    String number = value.substring(koma2 + 2);
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("emulator" + " favorites");
                    boolean stop = false;
                    for (int i = 0; stop == false; i++) {
                        Message message123 = messages.get(i);
                        if (i == (int) dataSnapshot.getChildrenCount() - 2)
                            stop = true;
                        if (message123.getText().equals(name)) {
                            stop = true;
                            messages.remove(message123);
                        } else if (stop == true) {
                            message123 = messages.get((int) dataSnapshot.getChildrenCount() - 1);
                            messages.remove(message123);
                        }
                        i++;
                    }
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener1);
            ImageView showMySettings = (ImageView) findViewById(R.id.mySettings);
            final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            showMySettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            });
            if (mapReady)
                m_map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapMain);
            mapFragment.getMapAsync(this);
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            dialog123 = new ProgressDialog(this);
            dialog123.setMessage("Searching for GPS");
            dialog123.setCanceledOnTouchOutside(false);
        }
        if(FirebaseAuth.getInstance().getCurrentUser() != null)
        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance()
                .getCurrentUser().getPhoneNumber().substring(3));
        else
            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
        mChildEventListener123 = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getValue().toString().replace("{text=deleted, name=", "").replace("}", "").equals("deleted")){
                    messages.clear();
                    messagesInApp.clear();
                    outsideAnApp.clear();
                    displayContacts();
                    dataSnapshot.getRef().removeValue();
                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mMessagesDatabaseReferences.addChildEventListener(mChildEventListener123);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapReady = true;
        m_map = googleMap;
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                GPSTrackerHelper gpsT1 = new GPSTrackerHelper(getApplicationContext(), "WIFI");
                if ((ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED && !isNetworkConnected())) {
                    m_map.setMyLocationEnabled(false);
                    EditText searchEdit = (EditText)findViewById(R.id.searchEdit);
                    String text = searchEdit.getText().toString();
                    if(!searchEdit.getText().toString().equals("") || searchEdit.getText().toString().equals("Search")) {
                        searchEdit.setText("");
                        searchEdit.setText(text);
                        searchEdit.setSelection(searchEdit.getText().toString().length());
                    }
                    allConts.clear();
                    TextView inAppMenu = (TextView)findViewById(R.id.insideAppMenu);
                    TextView outsideAppMenu = (TextView)findViewById(R.id.outsideAppMenu);
                    TextView allMenu = (TextView)findViewById(R.id.allContMenu);
                    TextView inAppTxt = (TextView)findViewById(R.id.inAppTxt);
                    TextView outsideAppTxt = (TextView)findViewById(R.id.outsideAppTxt);
                    RecyclerView allContsListView = (RecyclerView)findViewById(R.id.allContactsListView);
                    MessageAdapter allContactsAdapter2 = new MessageAdapter(allConts2);
                    inAppmMessageListView = (RecyclerView)findViewById(R.id.inAppContactsListView);
                    outsideAppmMessageListView = (RecyclerView)findViewById(R.id.outsideAnAppContactsListView);
                    inAppMenu.setVisibility(View.GONE);
                    outsideAppMenu.setVisibility(View.GONE);
                    TextView favoritesTxt = (TextView)findViewById(R.id.favoritesTxt);
                    RecyclerView favoritesListView = (RecyclerView)findViewById(R.id.favoritesListView);
                    favoritesTxt.setVisibility(View.GONE);
                    favoritesListView.setVisibility(View.GONE);
                    allMenu.setTextColor(Color.parseColor("#000000"));
                    inAppTxt.setVisibility(View.GONE);
                    outsideAppTxt.setVisibility(View.GONE);
                    inAppmMessageListView.setVisibility(View.GONE);
                    outsideAppmMessageListView.setVisibility(View.GONE);
                    MessageAdapter allContactsMessageAdapter = new MessageAdapter(allConts);
                    allContsListView.setAdapter(allContactsMessageAdapter);
                    RecyclerView.LayoutManager mLayoutManager5 = new LinearLayoutManager(getApplicationContext());
                    allContsListView.setLayoutManager(mLayoutManager5);
                    allContsListView.setVisibility(View.VISIBLE);
                    displayContacts2();
                    Toast toast = Toast.makeText(getApplicationContext(), "No Internet connection", Toast.LENGTH_SHORT);
                    if (toast == null || !toast.getView().isShown()) {
                        toast.show();
                    }
                } else if ((ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED && isNetworkConnected())) {
                    EditText searchEdit = (EditText)findViewById(R.id.searchEdit);
                    String text = searchEdit.getText().toString();
                    if(!searchEdit.getText().toString().equals("") || searchEdit.getText().toString().equals("Search")) {
                        searchEdit.setText("");
                        searchEdit.setText(text);
                        searchEdit.setSelection(searchEdit.getText().toString().length());
                    }
                        TextView favoritesTxt = (TextView)findViewById(R.id.favoritesTxt);
                        if(!favoritesArray.isEmpty()) {
                            favoritesTxt.setVisibility(View.VISIBLE);
                            favoritesListView.setVisibility(View.VISIBLE);
                        }
                        else {
                            favoritesTxt.setVisibility(View.GONE);
                            favoritesListView.setVisibility(View.GONE);
                        }
                        TextView inAppMenu = (TextView)findViewById(R.id.insideAppMenu);
                        TextView outsideAppMenu = (TextView)findViewById(R.id.outsideAppMenu);
                        TextView allMenu = (TextView)findViewById(R.id.allContMenu);
                        TextView inAppTxt = (TextView)findViewById(R.id.inAppTxt);
                        RecyclerView allContsListView = (RecyclerView)findViewById(R.id.allContactsListView);
                        allContsListView.setVisibility(View.GONE);
                        inAppmMessageListView = (RecyclerView)findViewById(R.id.inAppContactsListView);
                        outsideAppmMessageListView = (RecyclerView)findViewById(R.id.outsideAnAppContactsListView);
                        inAppmMessageListView.setVisibility(View.VISIBLE);
                        allMenu.setTextColor(Color.parseColor("#7B7A7A"));
                    outsideAppMenu.setTextColor(Color.parseColor("#7B7A7A"));
                        inAppTxt.setVisibility(View.VISIBLE);
                        inAppMenu.setVisibility(View.VISIBLE);
                        outsideAppMenu.setVisibility(View.VISIBLE);
                        m_map.setMyLocationEnabled(true);
                        CameraPosition curLoc = CameraPosition.builder().target(new LatLng(gpsT1.latitude, gpsT1.longitude)).zoom(14).build();
                        m_map.moveCamera(CameraUpdateFactory.newCameraPosition(curLoc));
                } else
                    return;
            }
        };

        GPSTrackerHelper gpsT1 = new GPSTrackerHelper(getApplicationContext(), "WIFI");
        m_map.setMyLocationEnabled(true);
        CameraPosition curLoc = CameraPosition.builder().target(new LatLng(gpsT1.latitude, gpsT1.longitude)).zoom(14).build();
        m_map.moveCamera(CameraUpdateFactory.newCameraPosition(curLoc));
    }
    public void onCreate3() {
        GPSTrackerHelper gpsT1 = new GPSTrackerHelper(getApplicationContext(), "GPS");
        GPSTrackerHelper gpsT11 = new GPSTrackerHelper(getApplicationContext(), "WIFI");
        if (isNetworkConnected()) {
            m_map.setMyLocationEnabled(true);
        }
        else if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED && turnedGPSChecked && !gpsT1.isGPSEnabled && !isNetworkConnected()) {
            TextView allContMenu = (TextView)findViewById(R.id.allContMenu);
            TextView outsideAppMenu = (TextView)findViewById(R.id.outsideAppMenu);
            TextView insideAppMenu = (TextView)findViewById(R.id.insideAppMenu);
            TextView inAppTxt = (TextView)findViewById(R.id.inAppTxt);
            TextView outsideAppTxt = (TextView)findViewById(R.id.outsideAppTxt);
            EditText searchEdit = (EditText)findViewById(R.id.searchEdit);
            final ColorStateList color = insideAppMenu.getTextColors();
            outsideAppMenu.setVisibility(View.GONE);
            insideAppMenu.setVisibility(View.GONE);
            inAppTxt.setVisibility(View.GONE);
            outsideAppTxt.setVisibility(View.GONE);
            outsideAppMenu.setTextColor(color);
            insideAppMenu.setTextColor(color);
            allContMenu.setTextColor(Color.parseColor("#000000"));
            inAppmMessageListView.setVisibility(View.GONE);
            outsideAppmMessageListView.setVisibility(View.GONE);
            searchEdit.setText(searchEdit.getText() + "");
            searchEdit.setSelection(searchEdit.getText().length());
            displayContacts();
            MessageAdapter allContactsMessageAdapter = new MessageAdapter(allConts);
            RecyclerView allContsListView = (RecyclerView)findViewById(R.id.allContactsListView);
            allContsListView.setAdapter(allContactsMessageAdapter);
            RecyclerView.LayoutManager mLayoutManager4 = new LinearLayoutManager(getApplicationContext());
            allContsListView.setLayoutManager(mLayoutManager4);
            if(isNetworkConnected())
                allContsListView.setVisibility(View.GONE);
            else
                allContsListView.setVisibility(View.VISIBLE);
        } else if (String.valueOf(gpsT1.latitude) == "0.0" && gpsT1.isGPSEnabled &&
                ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            dialog123.show();
            onGpsStatusChanged(1);
        } else if (String.valueOf(gpsT1.latitude) != "0.0" && gpsT1.isGPSEnabled &&
                ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            CameraPosition curLoc = CameraPosition.builder().target(new LatLng(gpsT1.latitude, gpsT1.longitude)).zoom(14).build();
            m_map.moveCamera(CameraUpdateFactory.newCameraPosition(curLoc));
        } else if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            showAccessPermissionAlert();
        } else if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED && turnedGPSChecked && !gpsT1.isGPSEnabled && isNetworkConnected()) {
            GPSTrackerHelper gpsT111 = new GPSTrackerHelper(getApplicationContext(), "WIFI");
            CameraPosition curLoc = CameraPosition.builder().target(new LatLng(gpsT11.latitude, gpsT111.longitude)).zoom(14).build();
            CameraPosition curLoc2 = CameraPosition.builder().target(new LatLng(gpsT11.latitude, gpsT111.longitude)).zoom(14).build();
            m_map.moveCamera(CameraUpdateFactory.newCameraPosition(curLoc2));
        }
        messagesInApp.clear();
        outsideAnApp.clear();
    }
    public void checkingNetwork(String by) {
        setTurnedGPSChecked(true);
        GPSTrackerHelper gps = new GPSTrackerHelper(this, "WIFI");
        if (by == "WIFI") {
            if (gps.isNetworkEnabled)
                return;
            else
                showSettingsAlertWifi();
        } else if (by == "GPS") {
            if (gps.isGPSEnabled)
                return;
            else {
                showSettingsAlertGPS();
            }
        } else if (by == "WIFI2") {
            gps.showSettingsAlertWifi2();
        } else if (by == "WIFI3") {
            gps.showSettingsAlertWifi3();
        }
    }
    public void showSettingsAlertWifi() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getApplicationContext());
        // Setting Dialog Title
        alertDialog.setTitle("WIFI is disabled");
        // Setting Dialog Message
        alertDialog.setMessage("Turn on WIFI to get currentLocation via WIFI.");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(intent);
                onCreate3();
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                onCreate3();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    public void showSettingsAlertGPS() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("GPS is not enabled");
        alertDialog.setCancelable(false);
        // Setting Dialog Message
        alertDialog.setMessage("Geting position via WiFi is not accurate. Turn on GPS for more accurate position.");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                onCreate3();
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                onCreate3();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    @Override
    public void onGpsStatusChanged(int event) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED)
            locationManager.addGpsStatusListener(this);
        switch (event) {
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                dialog123.dismiss();
                onCreate3();
                break;
            case GpsStatus.GPS_EVENT_STARTED:
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                break;
        }
    }
    public void showAccessPermissionAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Giving Permissions");
        // Setting Dialog Message
        alertDialog.setMessage("App can not get current currentLocation without using GPS and Network. And can not functionate without Reading Contacts and Reading Storage");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);
        // On pressing Settings button
        alertDialog.setPositiveButton("Give Permissions", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                askingPermissions1();
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    public void askingPermissions1() {
        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) ==
                PackageManager.PERMISSION_GRANTED) &&
                (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED)){
            Intent reboot = new Intent(getApplicationContext(), MapActivity.class);
            startActivity(reboot);
            finish();
    }
        else if ((ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) !=
                PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.READ_CONTACTS}, 2
            );
        }
        else if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 3
            );
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    askingPermissions1();
                } else {
                    showAccessPermissionAlert();
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults.length < 2) {
                    if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) ==
                            PackageManager.PERMISSION_GRANTED))
                        askingPermissions1();
                    else
                        showAccessPermissionAlert();
                } else if (grantResults.length > 1)
                    onCreate2();
                break;
            case 3:
                if (grantResults.length > 0 && grantResults.length < 3) {
                    if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) ==
                            PackageManager.PERMISSION_GRANTED))
                        askingPermissions1();
                    else
                        showAccessPermissionAlert();
                } else if (grantResults.length > 2)
                    onCreate2();
                break;
        }
    }
    public void setTurnedGPSChecked(boolean turnedGPSChecked) {
        this.turnedGPSChecked = turnedGPSChecked;
    }
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregisters BroadcastReceiver when app is destroyed.
        if (receiver != null && FirebaseAuth.getInstance().getCurrentUser() != null) {
            this.unregisterReceiver(receiver);
        }
    }
    // Refreshes the display if the network connection and the
    // pref settings allow it.
    @Override
    public void onStart() {
        super.onStart();
        // Gets the user's network preference settings
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        // Retrieves a string value for the preferences. The second parameter
        // is the default value to use if a preference value is not found.
        sPref = sharedPrefs.getString("listPref", "Wi-Fi");
        updateConnectedFlags();
    }
    // Checks the network connection and sets the wifiConnected and mobileConnected
    // variables accordingly.
    public void updateConnectedFlags() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
        if (activeInfo != null && activeInfo.isConnected()) {
            wifiConnected = activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
            mobileConnected = activeInfo.getType() == ConnectivityManager.TYPE_MOBILE;
        } else {
            wifiConnected = false;
            mobileConnected = false;
        }
    }
    public void displayContacts() {
        allConts.clear();
        messages.clear();
        messagesInApp.clear();
        messages2.clear();
        messagesInApp2.clear();
        outsideAnApp.clear();
        outsideAnApp2.clear();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                final String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        final String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        final String phoneNo2;
                        if(phoneNo.contains("+")) {
                            if(phoneNo.length() == 12)
                                phoneNo2 = phoneNo.replace("#", "").replace(".", "").replace("$", "").replace("]", "").substring(3).replaceAll(" ", "");
                            else {
                                phoneNo2 = phoneNo.replace("#", "").replace(".", "").replace("$", "").replace("]", "").substring(4).replaceAll(" ", "");
                            }
                        }
                        else {
                            phoneNo2 = phoneNo.replace("#", "").replace(".", "").replace("$", "").replace("]", "").replaceAll(" ", "");
                        }
                        final Message message = new Message(name, phoneNo2);
                        allConts.add(message);
                        messages.add(message);
                        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNo2.replace(" ", ""));
                            Query applesQuery1 = mMessagesDatabaseReferences.
                                    orderByChild("text").equalTo("kurva!");
                            applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(FirebaseAuth.getInstance().getCurrentUser() !=null)
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNo2.replace(" ", ""));
                                    int klmn = 0;
                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                        Message message1 = new Message(name, phoneNo2);
                                        messagesInApp.add(message1);
                                        inAppmMessageListView.setAdapter(inAppmMessageAdapter);
                                        klmn++;
                                    }
                                    if (klmn == 0) {
                                        if(FirebaseAuth.getInstance().getCurrentUser() !=null && phoneNo2.length() > 3)
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNo2.replace(" ", "").substring(3));
                                        Query applesQuery1 = mMessagesDatabaseReferences.
                                                orderByChild("text").equalTo("kurva!");
                                        applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if(FirebaseAuth.getInstance().getCurrentUser() !=null && phoneNo2.length() > 3)
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNo2.replace(" ", "").substring(3));
                                                int klmn = 0;
                                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                    Message message1 = new Message(name, phoneNo2);
                                                    messagesInApp.add(message1);
                                                    inAppmMessageListView.setAdapter(inAppmMessageAdapter);
                                                    klmn++;
                                                }
                                                if(klmn == 0){
                                                    Message message1 = new Message(name, phoneNo2);
                                                    outsideAnApp.add(message1);
                                                    outsideAppmMessageListView.setAdapter(outsideAppmMessageAdapter);
                                                }
                                            }
                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                        pCur.close();
                }
            }
        }
    }
    public void setSnapshot(DataSnapshot dataSnapshot1) {
        this.dataSnapshot1 = dataSnapshot1;
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return super.onNavigationItemSelected(item);
    }
    public void addNewContact(String Name, String Number){
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactInsertIndex = ops.size();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, Name) // Name of the person
                .build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(
                        ContactsContract.Data.RAW_CONTACT_ID,   rawContactInsertIndex)
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, Number) // Number of the person
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build()); // Type of mobile number
        try
        {
            ContentProviderResult[] res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        }
        catch (RemoteException e)
        {
            // error
        }
        catch (OperationApplicationException e)
        {
            // error
        }
    }
    public  boolean deleteContact(Context ctx, String phone, String name) {
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
        Cursor cur = ctx.getContentResolver().query(contactUri, null, null, null, null);
        try {
            if (cur.moveToFirst()) {
                do {
                    if (cur.getString(cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME)).equalsIgnoreCase(name)) {
                        String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
                        ctx.getContentResolver().delete(uri, null, null);
                        return true;
                    }
                } while (cur.moveToNext());
            }
        } catch (Exception e) {
        } finally {
            cur.close();
        }
        return false;
    }
    public String whichList(String name, String number){
        Message message = new Message(name, number);
        if(Arrays.asList(messages).contains(message)){
        }
        return "messages";
    }
    @Override
    protected void onStop() {
        DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(getApplicationContext());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("key", ""); //InputString: from the EditText
        editor.commit();
        if(mDictionary.getContactsCount() > 0) {
            Message message = new Message(mDictionary.getContact(1).getName(), mDictionary.getContact(1).getText());
            mDictionary.Clear();
            mDictionary.addContact(message);
        }
        Button allowButton = (Button)findViewById(R.id.allowButton);
        TextView askLocNumber = (TextView)findViewById(R.id.askLocNumber);
        String keyWord = "";
        if(FirebaseAuth.getInstance().getCurrentUser() != null) {
            boolean isAuthenticated = FirebaseAuth.getInstance().getCurrentUser() != null;
            if (isAuthenticated) {
                if (allowButton.getText().toString().equals("Stop Recieving")) {
                    keyWord = "recieving";
                    Toast.makeText(getApplicationContext(), "Stoped " + keyWord, Toast.LENGTH_SHORT).show();
                } else if (allowButton.getText().toString().equals("Stop Sharing")) {
                    keyWord = "sharing";
                    Toast.makeText(getApplicationContext(), "Stoped " + keyWord, Toast.LENGTH_SHORT).show();
                }
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location");
                    mMessagesDatabaseReferences.removeValue();
                    FirebaseDatabase.getInstance().getReference().child(askLocNumber.getText().toString().replaceAll(" ", "") + " Location").removeValue();
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                    Query applesQuery1 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Share loc");
                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    Query applesQuery3 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("number");
                    applesQuery3.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    Query applesQuery12 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Allow");
                    applesQuery12.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    askLocNumber = (TextView) findViewById(R.id.askLocNumber);
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(askLocNumber.getText().toString().replaceAll(" ",""));
                    Query applesQuery4 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Share loc");
                    applesQuery4.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    Query applesQuery5 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("number");
                    applesQuery5.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    Query applesQuery6 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("Allow");
                    applesQuery6.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    if (!askLocNumber.getText().toString().equals("+78 дєвочки, звоніть")) {
                        Message message1 = new Message("Stop", "it!");
                        mMessagesDatabaseReferences.push().setValue(message1);
                    }
                    RelativeLayout askLocLayout = (RelativeLayout) findViewById(R.id.askLocLayout);
                    TextView askLocLikeToAsk = (TextView) findViewById(R.id.askLocLiketoAsk);
                    Button refuseButton = (Button) findViewById(R.id.refuseButton);
                    allowButton = (Button) findViewById(R.id.allowButton);
                    allowButton.setText("Allow");
                    refuseButton.setVisibility(View.VISIBLE);
                    askLocLayout.setVisibility(View.GONE);
                    askLocLikeToAsk.setText("would like to know your current currentLocation");
                }
            }
        }
            super.onStop();
    }
    public void addNewItemArray(String name, String number){
        Message message1 = new Message(name, number);
        newItems.add(message1);
    }
    public String getNewItemName(int position){
        if (!newItems.isEmpty()) {
            return newItems.get(position).getName();
        }
        else
            return "";
    }
    public String getNewItemNumber(int position){
        if (!newItems.isEmpty()) {
            return newItems.get(position).getText();
        }
        else
            return "";
    }
    public void setTextq(String textq){
        this.textq = textq;
    }
    public String getTextq(){
        return textq;
    }
    public void displayContacts2() {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                final String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        final String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        final String phoneNo2 = phoneNo.replace("#", "").replace(".", "").replace("$", "").replace("]", "");
                        final Message message = new Message(name, phoneNo2);
                        allConts.add(message);
                    }
                }
            }
        }
    }
}
