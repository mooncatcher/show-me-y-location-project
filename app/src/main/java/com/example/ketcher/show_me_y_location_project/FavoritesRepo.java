package com.example.ketcher.show_me_y_location_project;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Ketcher on 19.12.2017.
 */

public class FavoritesRepo {

    private FavoriteCont favoriteCont;

        public FavoritesRepo(){

            favoriteCont = new FavoriteCont();

        }


        public static String createTable(){
            return "CREATE TABLE " + FavoriteCont.TABLE  + "("
                    + FavoriteCont.KEY_StudID  + " TEXT PRIMARY KEY  ,"
                    + FavoriteCont.KEY_Name + " TEXT, "
                    + FavoriteCont.KEY_MajorId  + " TEXT )";
        }



        public void insert(FavoriteCont favoriteCont) {
            SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
            ContentValues values = new ContentValues();
            values.put(FavoriteCont.KEY_StudID, favoriteCont.getStudentId());
            values.put(FavoriteCont.KEY_Name, favoriteCont.getName());
            values.put(FavoriteCont.KEY_MajorId, favoriteCont.getMajor());

            // Inserting Row
            db.insert(FavoriteCont.TABLE, null, values);
            DatabaseManager.getInstance().closeDatabase();
        }



        public void delete( ) {
            SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
            db.delete(FavoriteCont.TABLE, null,null);
            DatabaseManager.getInstance().closeDatabase();
        }







}
