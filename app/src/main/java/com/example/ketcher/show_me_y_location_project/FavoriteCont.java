package com.example.ketcher.show_me_y_location_project;

/**
 * Created by Ketcher on 19.12.2017.
 */

public class FavoriteCont {

    public static final String TAG = FavoriteCont.class.getSimpleName();
        public static final String TABLE = "Favorites";

        // Labels Table Columns names
        public static final String KEY_StudID = "FavoriteId";
        public static final String KEY_Name = "Name";
        public static final String KEY_Number = "Number";
        public static final String KEY_MajorId = "MajorId";

        private String ID ;
        private String number;
        private String name;
        private String majorId ;

        public String getStudentId() {
            return ID;
        }

        public void setStudentId(String ID) {
            this.ID = ID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMajor() {
            return majorId;
        }

        public void setMajor(String majorId) {
            this.majorId = majorId;
        }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
