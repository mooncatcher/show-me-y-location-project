package com.example.ketcher.show_me_y_location_project;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
/**
 * Created by Ketcher on 05.09.2017.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {
    private DatabaseReference mMessagesDatabaseReferences;
    private ChildEventListener mChildEventListener;
    View itemsView;
    String texxxt1;
    String userNumber = "";
    List<Message> messages = new ArrayList<>();
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView messageTextView, authorTextView;

        public MyViewHolder(final View view) {
            super(view);
            messageTextView = (TextView) view.findViewById(R.id.sayMyName);
            authorTextView = (TextView) view.findViewById(R.id.phoneNumber);
            final Button deleteContacts = (Button) view.findViewById(R.id.deleteContact);
            final Button askForLoc = (Button) view.findViewById(R.id.askForLoc);
            final Button shareLocWith = (Button) view.findViewById(R.id.shareLocWith);
            final ImageView ava = (ImageView) view.findViewById(R.id.ava);
            final ImageView isFavorite = (ImageView) view.findViewById(R.id.isFavorite);
            final ImageView isNotFavorite = (ImageView) view.findViewById(R.id.isNotFavorite);
            ConnectivityManager cm = (ConnectivityManager) view.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm.getActiveNetworkInfo() == null) {
                isFavorite.setVisibility(View.GONE);
                isNotFavorite.setVisibility(View.GONE);
            } else {

                isNotFavorite.setVisibility(View.VISIBLE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {


                    ConnectivityManager cm = (ConnectivityManager) view.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (cm.getActiveNetworkInfo() == null) {
                        if (deleteContacts.getVisibility() == View.GONE) {
                            deleteContacts.setText("Invite to app");
                            shareLocWith.setText("Share loc via SMS");
                            deleteContacts.setVisibility(View.VISIBLE);
                            shareLocWith.setVisibility(View.VISIBLE);
                        } else {
                            deleteContacts.setVisibility(View.GONE);
                            shareLocWith.setVisibility(View.GONE);
                        }
                    } else {
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                        mMessagesDatabaseReferences.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.child(authorTextView.getText().toString().replaceAll(" ", "")).exists()
                                        || dataSnapshot.child(authorTextView.getText().toString().replaceAll(" ", "")).exists()) {
                                    final DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(v.getContext());
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(v.getContext());
                                    String data = prefs.getString("key", null); //no id: default value
                                    if (!data.equals("")) {
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                        if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                            userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                        final TextView phoneNumber = (TextView) view.findViewById(R.id.phoneNumber);
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                        Query applesQuery1 = mMessagesDatabaseReferences.
                                                orderByChild("message");
                                        applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(final DataSnapshot dataSnapshot) {
                                                for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                    if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                                                        if (appleSnapshot.getValue().toString().contains(FirebaseAuth.getInstance()
                                                                .getCurrentUser().getPhoneNumber().substring(3)) ||
                                                                appleSnapshot.getValue().toString().contains(phoneNumber.getText().toString())) {
                                                            String data = appleSnapshot.getValue().toString();
                                                            String timestamp = data.substring(data.indexOf("timestamp"));
                                                            int koma = timestamp.indexOf(",");
                                                            timestamp = timestamp.substring(0, koma).replaceAll("[^0-9]", "");

                                                            long cutoff = new Date().getTime() - TimeUnit.MILLISECONDS.convert(10, TimeUnit.SECONDS);
                                                            //one
                                                            if (Long.parseLong(timestamp) < cutoff) {
                                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                                                                mMessagesDatabaseReferences.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(DataSnapshot dataSnapshot12) {
                                                                        if (dataSnapshot12.hasChild(phoneNumber.getText().toString() + " Location")
                                                                                || dataSnapshot12.hasChild(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location")) {
                                                                            Toast toast = Toast.makeText(v.getContext(), "User can't get your request now", Toast.LENGTH_LONG);
                                                                            if (toast == null || !toast.getView().isShown()) {
                                                                                toast.show();
                                                                            }
                                                                        } else {
                                                                            appleSnapshot.getRef().removeValue();
                                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(mDictionary.getContact(mDictionary.getContactsCount()).getText());
                                                                            Query applesQuery99 = mMessagesDatabaseReferences.
                                                                                    orderByChild("text").equalTo("Share loc");
                                                                            applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                @Override
                                                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                                        appleSnapshot.getRef().removeValue();
                                                                                    }
                                                                                }

                                                                                @Override
                                                                                public void onCancelled(DatabaseError databaseError) {
                                                                                }
                                                                            });
                                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNumber.getText().toString());
                                                                            Message message99 = new Message("Share loc", "");
                                                                            mMessagesDatabaseReferences.push().setValue(message99);
                                                                            NotificationHelper notificationHelper = new NotificationHelper();
                                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(authorTextView.getText().toString());
                                                                            Message message = new Message("", phoneNumber.getText().toString());
                                                                            mDictionary.addContact(message);
                                                                            notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "whould like to share currentLocation with you");
                                                                            Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {
                                                                    }
                                                                });
                                                            } else {
                                                                Toast toast = Toast.makeText(v.getContext(), "User can't get your request now", Toast.LENGTH_LONG);
                                                                if (toast == null || !toast.getView().isShown()) {
                                                                    toast.show();
                                                                }
                                                            }
                                                        } else {
                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNumber.getText().toString());
                                                            NotificationHelper notificationHelper = new NotificationHelper();
                                                            Message message = new Message("Share loc", "");
                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(authorTextView.getText().toString());
                                                            mMessagesDatabaseReferences.push().setValue(message);
                                                            Message message2 = new Message("", phoneNumber.getText().toString());
                                                            mDictionary.addContact(message2);
                                                            notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "whould like to share currentLocation with you");
                                                            Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                                        mMessagesDatabaseReferences.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (!dataSnapshot.hasChild("notificationRequests")) {
                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNumber.getText().toString());
                                                    Message message99 = new Message("Share loc", "");
                                                    mMessagesDatabaseReferences.push().setValue(message99);
                                                    NotificationHelper notificationHelper = new NotificationHelper();
                                                    Message message = new Message("", phoneNumber.getText().toString());
                                                    mDictionary.addContact(message);
                                                    notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "whould like to share currentLocation with you");
                                                    Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                    } else {
                                        if (deleteContacts.getVisibility() == View.GONE) {
                                            deleteContacts.setText("Delete contact");
                                            shareLocWith.setText("Share curr. loc. with");
                                            deleteContacts.setVisibility(View.VISIBLE);
                                            askForLoc.setVisibility(View.VISIBLE);
                                            shareLocWith.setVisibility(View.VISIBLE);
                                        } else {
                                            deleteContacts.setVisibility(View.GONE);
                                            askForLoc.setVisibility(View.GONE);
                                            shareLocWith.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    final DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(v.getContext());
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(v.getContext());
                                    String data = prefs.getString("key", null); //no id: default value
                                    if (!data.equals("")) {
                                        GPSTrackerHelper gpsTrackerHelper3 = new GPSTrackerHelper(view.getContext(), "GPS");
                                        if (gpsTrackerHelper3.isGPSEnabled && !String.valueOf(gpsTrackerHelper3.latitude).equals("0.0")) {
                                            GPSTrackerHelper gpsTrackerHelper = new GPSTrackerHelper(itemsView.getContext(), "GPS");
                                            TextView authorTextView = (TextView) itemsView.findViewById(R.id.phoneNumber);
                                            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                            smsIntent.setData(Uri.parse("sms:" + authorTextView.getText().toString()));
                                            smsIntent.putExtra("sms_body", gpsTrackerHelper.latitude + "," + gpsTrackerHelper.longitude);
                                            itemsView.getContext().startActivity(smsIntent);
                                        } else if (gpsTrackerHelper3.isNetworkEnabled) {
                                            GPSTrackerHelper gpsTrackerHelper = new GPSTrackerHelper(view.getContext(), "WIFI");
                                            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                            smsIntent.setData(Uri.parse("sms:" + authorTextView.getText().toString()));
                                            smsIntent.putExtra("sms_body", gpsTrackerHelper.latitude + "," + gpsTrackerHelper.longitude);
                                            view.getContext().startActivity(smsIntent);
                                        }
                                    } else {
                                        if (deleteContacts.getVisibility() == View.GONE) {
                                            deleteContacts.setText("Invite to app");
                                            shareLocWith.setText("Share loc via SMS");
                                            deleteContacts.setVisibility(View.VISIBLE);
                                            shareLocWith.setVisibility(View.VISIBLE);
                                        } else {
                                            deleteContacts.setVisibility(View.GONE);
                                            shareLocWith.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                        setTexxxt(messageTextView.getText().toString());
                        ImageView ava = (ImageView) view.findViewById(R.id.ava);
                        final ImageView isNotFavorite = (ImageView) view.findViewById(R.id.isNotFavorite);
                        final ImageView isFavorite = (ImageView) view.findViewById(R.id.isFavorite);
                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(authorTextView.getText().toString());
                        if (FirebaseAuth.getInstance().getCurrentUser() != null)
                            userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                    }
                    return;
                }
            });

            DBHelper dbHelper = new DBHelper(view.getContext());

            Toast.makeText(view.getContext(), messages.get(0).getName() + " /// " +
                    dbHelper.getAllContacts2().get(0).getText(), Toast.LENGTH_SHORT).show();
            /*
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " favorites");
            }
            mChildEventListener1 = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    int koma = dataSnapshot.getValue().toString().indexOf(",");
                    String name = dataSnapshot.getValue().toString().substring(0, koma).replace("{text=", "");
                    Query applesQuery99 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo(messageTextView.getText().toString());
                    applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            mMessagesDatabaseReferences = mFirebaseDatabase.getInstance().getReference().child(userNumber + " favorites");
                            int klmn = 0;
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                klmn++;
                            }
                            if (klmn == 0) {
                                isFavorite.setVisibility(View.GONE);
                                isNotFavorite.setVisibility(View.VISIBLE);
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            mMessagesDatabaseReferences.addChildEventListener(mChildEventListener1);
            */

            final DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(view.getContext());
            isFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isFavorite.setVisibility(View.GONE);
                    isNotFavorite.setVisibility(View.VISIBLE);
                    String userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);

                    /*
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(userNumber + " favorites");
                    Query applesQuery1 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo(messageTextView.getText().toString());
                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            });
            */


                    isNotFavorite.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isFavorite.setVisibility(View.VISIBLE);
                            isNotFavorite.setVisibility(View.GONE);
                            DBHelper dbHelper = new DBHelper(view.getContext());
                            Message addToFavorite = new Message(messageTextView.getText().toString(), authorTextView.getText().toString());
                            dbHelper.addContact(addToFavorite);

                        }
                    });
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                    mChildEventListener = new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                    .child(authorTextView.getText().toString());
                            Query applesQuery99 = mMessagesDatabaseReferences.
                                    orderByChild("text").equalTo("DisplayPhoto");
                            applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    int klmn = 0;
                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                        String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                        Picasso.with(view.getContext()).load
                                                (value).fit().centerCrop().into(ava);
                                        klmn++;
                                    }
                                    if (klmn == 0) {
                                        if (authorTextView.getText().toString().length() > 3) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(authorTextView.getText().toString());
                                        } else
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(authorTextView.getText().toString());
                                        Query applesQuery99 = mMessagesDatabaseReferences.
                                                orderByChild("text").equalTo("DisplayPhoto");
                                        applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                    String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                                                    Picasso.with(view.getContext()).load
                                                            (value).fit().centerCrop().into(ava);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    };
                    mMessagesDatabaseReferences.addChildEventListener(mChildEventListener);
                    shareLocWith.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            if (shareLocWith.getText().toString().equals("Share loc via SMS")) {
                                GPSTrackerHelper gpsTrackerHelper3 = new GPSTrackerHelper(view.getContext(), "GPS");
                                if (gpsTrackerHelper3.isGPSEnabled && !String.valueOf(gpsTrackerHelper3.latitude).equals("0.0")) {
                                    GPSTrackerHelper gpsTrackerHelper = new GPSTrackerHelper(itemsView.getContext(), "GPS");
                                    TextView authorTextView = (TextView) itemsView.findViewById(R.id.phoneNumber);
                                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                    smsIntent.setData(Uri.parse("sms:" + authorTextView.getText().toString()));
                                    smsIntent.putExtra("sms_body", gpsTrackerHelper.latitude + "," + gpsTrackerHelper.longitude);
                                    itemsView.getContext().startActivity(smsIntent);
                                } else if (gpsTrackerHelper3.isNetworkEnabled) {
                                    GPSTrackerHelper gpsTrackerHelper = new GPSTrackerHelper(view.getContext(), "WIFI");
                                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                    smsIntent.setData(Uri.parse("sms:" + authorTextView.getText().toString()));
                                    smsIntent.putExtra("sms_body", gpsTrackerHelper.latitude + "," + gpsTrackerHelper.longitude);
                                    view.getContext().startActivity(smsIntent);
                                }
                            } else if (shareLocWith.getText().toString().equals("Share curr. loc. with")) {
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");

                                final TextView phoneNumber = (TextView) view.findViewById(R.id.phoneNumber);
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                Query applesQuery1 = mMessagesDatabaseReferences.
                                        orderByChild("message");
                                applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot dataSnapshot) {
                                        for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            if (appleSnapshot.getValue().toString().contains(FirebaseAuth.getInstance()
                                                    .getCurrentUser().getPhoneNumber().substring(3)) ||
                                                    appleSnapshot.getValue().toString().contains(phoneNumber.getText().toString())) {
                                                String data = appleSnapshot.getValue().toString();
                                                String timestamp = data.substring(data.indexOf("timestamp"));
                                                int koma = timestamp.indexOf(",");
                                                timestamp = timestamp.substring(0, koma).replaceAll("[^0-9]", "");
                                                timestamp = timestamp.substring(0, 13);
                                                long cutoff = new Date().getTime() - TimeUnit.MILLISECONDS.convert(10, TimeUnit.SECONDS);
                                                //one
                                                if (Long.parseLong(timestamp) < cutoff) {
                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                                                    mMessagesDatabaseReferences.addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot12) {
                                                            if (dataSnapshot12.hasChild(phoneNumber.getText().toString() + " Location")
                                                                    || dataSnapshot12.hasChild(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location") ||
                                                                    dataSnapshot12.hasChild((phoneNumber.getText().toString() + " Locatione"))) {
                                                                Toast toast = Toast.makeText(v.getContext(), "User can't get your request now", Toast.LENGTH_LONG);
                                                                if (toast == null || !toast.getView().isShown()) {
                                                                    toast.show();
                                                                }
                                                            } else {
                                                                appleSnapshot.getRef().removeValue();
                                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(mDictionary.getContact(mDictionary.getContactsCount()).getText());
                                                                Query applesQuery99 = mMessagesDatabaseReferences.
                                                                        orderByChild("text").equalTo("Share loc");
                                                                applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                            appleSnapshot.getRef().removeValue();
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {
                                                                    }
                                                                });
                                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNumber.getText().toString());
                                                                Message message99 = new Message("Share loc", "");
                                                                mMessagesDatabaseReferences.push().setValue(message99);
                                                                NotificationHelper notificationHelper = new NotificationHelper();
                                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(authorTextView.getText().toString());
                                                                Message message = new Message("", phoneNumber.getText().toString());
                                                                mDictionary.addContact(message);
                                                                notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "whould like to share currentLocation with you");
                                                                Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {
                                                        }
                                                    });
                                                } else {
                                                    Toast toast = Toast.makeText(v.getContext(), "User can't get your request now", Toast.LENGTH_LONG);
                                                    if (toast == null || !toast.getView().isShown()) {
                                                        toast.show();
                                                    }
                                                }
                                            } else {
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNumber.getText().toString());
                                                NotificationHelper notificationHelper = new NotificationHelper();
                                                Message message = new Message("Share loc", "");
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(authorTextView.getText().toString());
                                                mMessagesDatabaseReferences.push().setValue(message);
                                                Message message2 = new Message("", phoneNumber.getText().toString());
                                                mDictionary.addContact(message2);
                                                notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "whould like to share currentLocation with you");
                                                Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                                mMessagesDatabaseReferences.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (!dataSnapshot.hasChild("notificationRequests")) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNumber.getText().toString());
                                            Message message99 = new Message("Share loc", "");
                                            mMessagesDatabaseReferences.push().setValue(message99);
                                            NotificationHelper notificationHelper = new NotificationHelper();
                                            Message message = new Message("", phoneNumber.getText().toString());
                                            mDictionary.addContact(message);
                                            notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "whould like to share currentLocation with you");
                                            Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                        /*
                        Query applesQuery1 = mMessagesDatabaseReferences.
                                orderByChild("message").equalTo(mDictionary.getContact(1).getName().replaceAll(" ", "") + "/"
                                + FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3).replaceAll(" ", ""));
                        applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                int klmn1 = 0;
                                for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    klmn1++;
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    Query applesQuery99 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow");
                                    applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            int klmn = 0;
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                Toast.makeText(v.getContext(), "At first stop your current currentLocation sharing/recieving", Toast.LENGTH_SHORT).show();
                                                klmn++;
                                            }
                                            if (klmn == 0) {
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                                Query applesQuery1 = mMessagesDatabaseReferences.
                                                        orderByChild("text").equalTo("number");
                                                applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        int klmn = 0;
                                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                            Toast.makeText(v.getContext(), "At first stop your current currentLocation sharing/recieving", Toast.LENGTH_SHORT).show();
                                                            klmn++;
                                                        }
                                                        if (klmn == 0) {
                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                                            if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                                                userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                                            {
                                                                String number = authorTextView.getText().toString();
                                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                                                Query applesQuery13 = mMessagesDatabaseReferences.
                                                                        orderByChild("PhoneNumber").equalTo(number.replaceAll("[^0-9]", ""));
                                                                applesQuery13.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                                        int klmn = 0;
                                                                        for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                            Toast.makeText(v.getContext(), "User can't get your request now. Try later", Toast.LENGTH_LONG).show();
                                                                            klmn++;
                                                                        }
                                                                        if (klmn == 0) {
                                                                            {
                                                                                NotificationHelper notificationHelper = new NotificationHelper();
                                                                                Message message = new Message("Share loc", "");
                                                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(authorTextView.getText().toString());
                                                                                mMessagesDatabaseReferences.push().setValue(message);
                                                                                notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "whould like to share currentLocation with you");
                                                                            }
                                                                        }
                                                                    }
                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                    }
                                                });
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                }
                                if (klmn1 == 0) {
                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                    Query applesQuery99 = mMessagesDatabaseReferences.
                                            orderByChild("text").equalTo("Allow");
                                    applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            int klmn = 0;
                                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                Toast.makeText(v.getContext(), "At first, stop your current currentLocation sharing/recieving", Toast.LENGTH_SHORT).show();
                                                klmn++;
                                            }
                                            if (klmn == 0) {
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                                if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                                    userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                                {
                                                    String number = authorTextView.getText().toString();
                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                                    Query applesQuery13 = mMessagesDatabaseReferences.
                                                            orderByChild("PhoneNumber").equalTo(number.replaceAll("[^0-9]", ""));
                                                    applesQuery13.addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            int klmn = 0;
                                                            for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                Toast.makeText(v.getContext(), "User can't get your request now. Try later", Toast.LENGTH_LONG).show();
                                                                klmn++;
                                                            }
                                                            if (klmn == 0) {
                                                                {
                                                                    NotificationHelper notificationHelper = new NotificationHelper();
                                                                    Message message = new Message("Share loc", "");
                                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(authorTextView.getText().toString());
                                                                    mMessagesDatabaseReferences.push().setValue(message);
                                                                    notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "whould like to share currentLocation with you");
                                                                }
                                                            }
                                                        }
                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }
            });
        */
                            }
                        }
                    });
                    deleteContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (deleteContacts.getText().equals("Delete contact")) {
                                MapActivity mapActivity = new MapActivity();
                                mapActivity.deleteContact(view.getContext(), authorTextView.getText().toString(), messageTextView.getText().toString());
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance()
                                        .getCurrentUser().getPhoneNumber().substring(3));
                                Message message = new Message("deleted", "deleted");
                                mMessagesDatabaseReferences.push().setValue(message);
                            } else if (deleteContacts.getText().toString().equals("Invite to app")) {
                                Intent toMessage = new Intent(Intent.ACTION_VIEW);
                                toMessage.setData(Uri.parse("sms:" + authorTextView.getText().toString()));
                                toMessage.putExtra("sms_body", "Try SMYL app: LINK");
                                v.getContext().startActivity(toMessage);
                            }
                        }
                    });
                    askForLoc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");

                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                            final DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(v.getContext());
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                            Query applesQuery1 = mMessagesDatabaseReferences.
                                    orderByChild("message");
                            final TextView phoneNumber = (TextView) view.findViewById(R.id.phoneNumber);
                            applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                        if (appleSnapshot.getValue().toString().contains(FirebaseAuth.getInstance()
                                                .getCurrentUser().getPhoneNumber().substring(3)) ||
                                                appleSnapshot.getValue().toString().contains(phoneNumber.getText().toString())) {
                                            String data = appleSnapshot.getValue().toString();
                                            String timestapm = data.substring(data.indexOf("timestamp"));

                                            timestapm = timestapm.replaceAll("[^0-9]", "");
                                            timestapm = timestapm.substring(0, 13);
                                            long cutoff = new Date().getTime() - TimeUnit.MILLISECONDS.convert(10, TimeUnit.SECONDS);
                                            if (Long.parseLong(timestapm) < cutoff) {
                                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                                                mMessagesDatabaseReferences.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
//two
                                                        if (dataSnapshot.hasChild(phoneNumber.getText().toString() + " Location")
                                                                || dataSnapshot.hasChild(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3) + " Location") ||
                                                                dataSnapshot.hasChild((phoneNumber.getText().toString() + " Locatione"))) {
                                                            Toast toast = Toast.makeText(v.getContext(), "User can't get your request now", Toast.LENGTH_LONG);
                                                            if (toast == null || !toast.getView().isShown()) {
                                                                toast.show();
                                                            }
                                                        } else {
                                                            appleSnapshot.getRef().removeValue();
                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(mDictionary.getContact(mDictionary.getContactsCount()).getText());
                                                            Query applesQuery99 = mMessagesDatabaseReferences.
                                                                    orderByChild("text").equalTo("Share loc");
                                                            applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                        appleSnapshot.getRef().removeValue();
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {
                                                                }
                                                            });
                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(phoneNumber.getText().toString());
                                                            NotificationHelper notificationHelper = new NotificationHelper();
                                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(authorTextView.getText().toString());
                                                            Message message = new Message("", phoneNumber.getText().toString());
                                                            mDictionary.addContact(message);
                                                            notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "Asking for your currentLocation");
                                                            Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                    }
                                                });
                                            } else {
                                                Toast toast = Toast.makeText(v.getContext(), "User can't get your request now", Toast.LENGTH_LONG);
                                                if (toast == null || !toast.getView().isShown()) {
                                                    toast.show();
                                                }
                                            }
                                        } else {
                                            NotificationHelper notificationHelper = new NotificationHelper();
                                            Message message = new Message("", phoneNumber.getText().toString());
                                            mDictionary.addContact(message);
                                            notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "Asking for Your Location");
                                            Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                            mMessagesDatabaseReferences.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (!dataSnapshot.hasChild("notificationRequests")) {
                                        NotificationHelper notificationHelper = new NotificationHelper();
                                        Message message = new Message("", phoneNumber.getText().toString());
                                        mDictionary.addContact(message);
                                        notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "Asking for Your Location");
                                        Toast.makeText(v.getContext(), "Request sent successfully", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                    /*
                    Query applesQuery1 = mMessagesDatabaseReferences.
                            orderByChild("message").equalTo(mDictionary.getContact(1).getName().replaceAll(" ", "") + "/" + FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                    applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int klmn1 = 0;
                            for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                klmn1++;
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                Query applesQuery99 = mMessagesDatabaseReferences.
                                        orderByChild("text").equalTo("Allow");
                                applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        int klmn = 0;
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            Toast.makeText(v.getContext(), "At first, stop your current currentLocation sharing/recieving", Toast.LENGTH_SHORT).show();
                                            klmn++;
                                        }
                                        if (klmn == 0) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                            Query applesQuery1 = mMessagesDatabaseReferences.
                                                    orderByChild("text").equalTo("number");
                                            applesQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    int klmn = 0;
                                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                        Toast.makeText(v.getContext(), "At first stop your current currentLocation sharing/recieving", Toast.LENGTH_SHORT).show();
                                                        klmn++;
                                                    }
                                                    if (klmn == 0) {
                                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                                        Query applesQuery99 = mMessagesDatabaseReferences.
                                                                orderByChild("text").equalTo("Allow");
                                                        applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                int klmn = 0;
                                                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                    Toast.makeText(v.getContext(), "At first, stop your current currentLocation sharing/recieving", Toast.LENGTH_SHORT).show();
                                                                    klmn++;
                                                                }
                                                                if (klmn == 0) {
                                                      //here
                                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                                                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                                                        userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                                                    {
                                                                        String number = authorTextView.getText().toString();
                                                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                                                        Query applesQuery13 = mMessagesDatabaseReferences.
                                                                                orderByChild("PhoneNumber").equalTo(number.replaceAll("[^0-9]", ""));
                                                                        applesQuery13.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                            @Override
                                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                                int klmn = 0;
                                                                                for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                                    Toast.makeText(v.getContext(), "User can't get your request now. Try later", Toast.LENGTH_LONG).show();
                                                                                    klmn++;
                                                                                }
                                                                                if (klmn == 0) {
                                                                                    NotificationHelper notificationHelper = new NotificationHelper();
                                                                                    notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "Asking for Your Location");
                                                                                }
                                                                            }
                                                                            @Override
                                                                            public void onCancelled(DatabaseError databaseError) {
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {
                                                            }
                                                        });
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                            }
                            if (klmn1 == 0) {
                                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                Query applesQuery99 = mMessagesDatabaseReferences.
                                        orderByChild("text").equalTo("Allow");
                                applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        int klmn = 0;
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            Toast.makeText(v.getContext(), "At first, stop your current currentLocation sharing/recieving", Toast.LENGTH_SHORT).show();
                                            klmn++;
                                        }
                                        if (klmn == 0) {
                                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                    .child(authorTextView.getText().toString().replaceAll(" ", ""));
                                            Query applesQuery999 = mMessagesDatabaseReferences.
                                                    orderByChild("text").equalTo("DisplayName");
                                            applesQuery999.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot nameSnapshot : dataSnapshot.getChildren()){
                                                        final String name = nameSnapshot.getValue().toString().replace("{text=DisplayName, name=", "").replace("}", "");
                                                        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                                                .child("notificationRequests"); //z plus
                                                        Query applesQuery99 = mMessagesDatabaseReferences.
                                                                orderByChild("message").equalTo(name + "/" + authorTextView.getText().toString().replaceAll(" ", ""));
                                                        applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                int klmn = 0;
                                                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                    appleSnapshot.getRef().removeValue();
                                                                }
                                                                Query applesQuery99 = mMessagesDatabaseReferences.
                                                                        orderByChild("PhoneNumber").equalTo(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                                                                applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                                        int klmn = 0;
                                                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                            appleSnapshot.getRef().removeValue();
                                                                        }
                                                                    }
                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {
                                                                    }
                                                                });
                                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                                                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                                                                        userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
                                                                {
                                                                    String number = authorTextView.getText().toString();
                                                                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child("notificationRequests");
                                                                    Query applesQuery13 = mMessagesDatabaseReferences.
                                                                            orderByChild("PhoneNumber").equalTo(number.replaceAll("[^0-9]", ""));
                                                                    applesQuery13.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                                            int klmn = 0;
                                                                            for (final DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                                                Toast.makeText(v.getContext(), "User can't get your request now. Try later", Toast.LENGTH_LONG).show();
                                                                                klmn++;
                                                                            }
                                                                            if (klmn == 0) {
                                                                                NotificationHelper notificationHelper = new NotificationHelper();
                                                                                notificationHelper.sendNotificationToUser(authorTextView.getText().toString(), "Asking for Your Location");
                                                                            }
                                                                        }
                                                                        @Override
                                                                        public void onCancelled(DatabaseError databaseError) {
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {
                                                            }
                                                        });
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            });
        }
    }
*/
                        }
                    });
                }
            });
        }
    }

    public MessageAdapter(List<Message> messages) {
        this.messages = messages;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contacts_items, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Message message = messages.get(position);
        holder.messageTextView.setText(message.getText());
        holder.authorTextView.setText(message.getName());
    }
    @Override
    public int getItemCount() {
        return messages.size();
    }
    public void setTexxxt(String texxxt1) {
        this.texxxt1 = texxxt1;
    }
    public String getTexxxt() {
        return texxxt1;
    }

}
