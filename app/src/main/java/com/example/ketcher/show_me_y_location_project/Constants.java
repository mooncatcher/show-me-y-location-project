package com.example.ketcher.show_me_y_location_project;
/**
 * Created by Ketcher on 18.08.2017.
 */
class Constants {
    public static final String ACCOUNT_TYPE = "com.example.ajay.contacts_4";
    public static final String ACCOUNT_NAME = "MyAccount";
    public static final String ACCOUNT_TOKEN = "12345";
//    public static final String AUTHTOKEN_TYPE_READ_ONLY = "Read only";
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
}
