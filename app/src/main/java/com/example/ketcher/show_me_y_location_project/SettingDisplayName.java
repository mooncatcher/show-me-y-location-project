package com.example.ketcher.show_me_y_location_project;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
/**
 * Created by Ketcher on 06.10.2017.
 */
public class SettingDisplayName extends AppCompatActivity {
    private FirebaseDatabase mFirebaseDatabase;
    ChildEventListener mChildEventLister;
    private DatabaseReference mMessagesDatabaseReferences;
    private  StorageReference mStorageRefences;
    ProgressDialog mProgressiveDialog;
    FirebaseAuth mFirebaseAuth;
    private ChildEventListener mChildEventListener;
    private static final int GALLERY_INTENT = 2;
    ImageView setImg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_display_name);
        final EditText settingDisplayName = (EditText)findViewById(R.id.displayNameEdit);
        Button apply = (Button)findViewById(R.id.apply);
        final ImageView setImg = (ImageView)findViewById(R.id.setImg);
        mProgressiveDialog = new ProgressDialog(this);
        setImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toGalery = new Intent(Intent.ACTION_PICK);
                toGalery.setType("image/*");
                startActivityForResult(toGalery, GALLERY_INTENT);
            }
        });
        mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
        mChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                        .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                Query applesQuery99 = mMessagesDatabaseReferences.
                        orderByChild("text").equalTo("DisplayPhoto");
                applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                            String value = appleSnapshot.getValue().toString().replace("{text=DisplayPhoto, name=", "").replace("}", "");
                            Picasso.with(SettingDisplayName.this).load
                                    (value).fit().centerCrop().into(setImg);
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mMessagesDatabaseReferences.addChildEventListener(mChildEventListener);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = mFirebaseAuth.getCurrentUser();
        String userNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);
        mMessagesDatabaseReferences = mFirebaseDatabase.getReference().child(userNumber);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Query isNameAlreadyExists = mMessagesDatabaseReferences.
                        orderByChild("text").equalTo("DisplayName");
                isNameAlreadyExists.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot IsNameAlreadyExists : dataSnapshot.getChildren()) {
                            IsNameAlreadyExists.getRef().removeValue();
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                Message message = new Message("DisplayName", settingDisplayName.getText().toString());
                Message message1 = new Message(settingDisplayName.getText().toString(), "DisplayName");
                mMessagesDatabaseReferences.push().setValue(message);
                DictionaryOpenHelper mDictionary = new DictionaryOpenHelper(getApplicationContext());
                mDictionary.Clear();
                mDictionary.addContact(message1);
                Intent startApp = new Intent(getApplicationContext(), MapActivity.class);
                startActivity(startApp);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_INTENT && resultCode == RESULT_OK){
            mProgressiveDialog.setTitle("Uploading...");
            mProgressiveDialog.show();
            Uri uri = data.getData();
            StorageReference filepath = FirebaseStorage.getInstance().getReference()
                    .child("Photos").child(uri.getLastPathSegment());
            mStorageRefences = FirebaseStorage.getInstance().getReference();
            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    mProgressiveDialog.dismiss();
                    Uri downloadUri = taskSnapshot.getDownloadUrl();
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3));
                    Query applesQuery99 = mMessagesDatabaseReferences.
                            orderByChild("text").equalTo("DisplayPhoto");
                    applesQuery99.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    Message message = new Message("DisplayPhoto", downloadUri.toString());
                    mMessagesDatabaseReferences.push().setValue(message);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        }
    }
}
