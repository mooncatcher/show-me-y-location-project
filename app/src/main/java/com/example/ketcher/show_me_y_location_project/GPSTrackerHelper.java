package com.example.ketcher.show_me_y_location_project;
import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
/**
 * Created by Ketcher on 25.07.2017.
 */
public class GPSTrackerHelper implements LocationListener {
    private static final String TAG = "GPSTrackerHelper";
    private final Context context;
    // flag for GPS status
    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    Location currentLocation; // currentLocation
    double latitude; // latitude
    double longitude; // longitude
    private ChildEventListener mChildEventListener;
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 50;
    private DatabaseReference mMessagesDatabaseReferences;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 0;
    // Declaring a Location Manager
    protected LocationManager locationManager;
    public GPSTrackerHelper(Context context, String what) {
        this.context = context;
        getLocation(what);
    }
    public Location getLocation(String provider) {
        try {
            locationManager = (LocationManager) context
                    .getSystemService(Service.LOCATION_SERVICE);
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            // getting network status
            if (!isGPSEnabled && !isNetworkEnabled) {
                Log.i(TAG, "gpd and network is not enabled, do nothing");
                this.canGetLocation = false;
            } else {
                this.canGetLocation = true;
                // First get currentLocation from GPS
                if (provider.equals("WIFI")) {
                    if (currentLocation == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        if (locationManager != null) {
                            currentLocation = locationManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (currentLocation != null) {
                                latitude = currentLocation.getLatitude();
                                longitude = currentLocation.getLongitude();
                            }
                        }
                    }
                }
            }
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                if (provider.equals("GPS")) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager != null) {
                        currentLocation = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (currentLocation != null) {
                            latitude = currentLocation.getLatitude();
                            longitude = currentLocation.getLongitude();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentLocation;
    }
    @Override
    public void onLocationChanged(Location location) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getCurrentUser()
                    .getPhoneNumber().substring(3));
            Query canShareLocationQuery = mMessagesDatabaseReferences.
                    orderByChild("text").equalTo("Share loc");
            canShareLocationQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mMessagesDatabaseReferences = FirebaseDatabase.getInstance()
                            .getReference()
                            .child(FirebaseAuth.getInstance().getCurrentUser()
                                    .getPhoneNumber().substring(3));
                    boolean userAllowedToShareYourLocation = false;
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        userAllowedToShareYourLocation = true;
                    }
                    if (!userAllowedToShareYourLocation) {
                        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                    .child(FirebaseAuth.getInstance()
                                            .getCurrentUser()
                                            .getPhoneNumber()
                                            .substring(3) + " Location");
                        } else {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference();
                        }
                        Query myLocationQuery = mMessagesDatabaseReferences.
                                orderByChild("text").equalTo("MyLoc");
                        myLocationQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                            mMessagesDatabaseReferences = FirebaseDatabase.getInstance().getReference()
                                    .child(FirebaseAuth.getInstance()
                                            .getCurrentUser()
                                            .getPhoneNumber().substring(3) + " Location");
                        }
                        Query allowedSomeoneToSeeMyLocationQuery = mMessagesDatabaseReferences.
                                orderByChild("text").equalTo("Allowed");
                        allowedSomeoneToSeeMyLocationQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Message message = new Message("MyLoc", String.valueOf(longitude) + "," + String.valueOf(latitude));
                                    mMessagesDatabaseReferences.push().setValue(message);
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }
    @Override
    public void onProviderDisabled(String provider) {
    }
    @Override
    public void onProviderEnabled(String provider) {
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
    public double getLatitude() {
        if (currentLocation != null) {
            latitude = currentLocation.getLatitude();
        }
        // return latitude
        return latitude;
    }
    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (currentLocation != null) {
            longitude = currentLocation.getLongitude();
        }
        // return longitude
        return longitude;
    }
    public boolean canGetLocation() {
        return this.canGetLocation;
    }
    /**
     * Function to show settings alert dialog
     */
    public void showSettingsAlertGPS() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        // Setting Dialog Title
        alertDialog.setTitle("GPS is not enabled");
        // Setting Dialog Message
        alertDialog.setMessage("Geting position via WiFi is not accurate. Turn on GPS for more accurate position.");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    public void showSettingsAlertWifi2() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        // Setting Dialog Title
        alertDialog.setTitle("Can't get Location");
        // Setting Dialog Message
        alertDialog.setMessage("You must enable GPS or *хоча б*)) Wi-fi for getting current currentLocation");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    public void showSettingsAlertWifi3() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        // Setting Dialog Title
        alertDialog.setTitle("Can't get Location");
        // Setting Dialog Message
        alertDialog.setMessage("Because of your Location Settings you can get your current currentLocation only via GPS");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    public void showSettingsAlertWifi() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        // Setting Dialog Title
        alertDialog.setTitle("WIFI is not enabled");
        // Setting Dialog Message
        alertDialog.setMessage("Can't get current currentLocation via WI-FI if WI-FI is disabled.");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                context.startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTrackerHelper.this);
        }
    }
    public void toast(String s, String lenghth) {
        if (lenghth == "short")
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
        if (lenghth == "long")
            Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }
}
